package p002eu.chainfire.p005b;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import p002eu.chainfire.p005b.C0070d.C0071a;
import p002eu.chainfire.p005b.C0070d.C0072b;

/* renamed from: eu.chainfire.b.c */
public class C0032c {

    /* renamed from: a */
    protected static final String[] f125a = {"echo -BOC-", "id"};
    /* access modifiers changed from: private */

    /* renamed from: b */
    public static volatile boolean f126b = true;

    /* renamed from: eu.chainfire.b.c$a */
    public static class C0033a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public Handler f127a = null;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public boolean f128b = true;
        /* access modifiers changed from: private */

        /* renamed from: c */
        public String f129c = "sh";
        /* access modifiers changed from: private */

        /* renamed from: d */
        public boolean f130d = false;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public boolean f131e = true;
        /* access modifiers changed from: private */

        /* renamed from: f */
        public boolean f132f = true;
        /* access modifiers changed from: private */

        /* renamed from: g */
        public List<C0034b> f133g = new LinkedList();
        /* access modifiers changed from: private */

        /* renamed from: h */
        public Map<String, String> f134h = new HashMap();
        /* access modifiers changed from: private */

        /* renamed from: i */
        public C0071a f135i = null;
        /* access modifiers changed from: private */

        /* renamed from: j */
        public C0071a f136j = null;
        /* access modifiers changed from: private */

        /* renamed from: k */
        public int f137k = 0;

        /* access modifiers changed from: private */
        /* renamed from: a */
        public C0067u m120a(C0055m mVar, boolean z) {
            return VERSION.SDK_INT >= 19 ? new C0069v(this, mVar, z) : new C0067u(this, mVar, z);
        }

        /* renamed from: a */
        public C0033a mo106a() {
            return mo112a((String) "sh");
        }

        /* renamed from: a */
        public C0033a mo107a(int i) {
            this.f137k = i;
            return this;
        }

        /* renamed from: a */
        public C0033a mo108a(Handler handler) {
            this.f127a = handler;
            return this;
        }

        /* renamed from: a */
        public C0033a mo109a(C0071a aVar) {
            this.f135i = aVar;
            return this;
        }

        /* renamed from: a */
        public C0033a mo110a(Object obj) {
            return mo111a(obj, 0, (C0054l) null);
        }

        /* renamed from: a */
        public C0033a mo111a(Object obj, int i, C0054l lVar) {
            this.f133g.add(new C0034b(obj, i, lVar));
            return this;
        }

        /* renamed from: a */
        public C0033a mo112a(String str) {
            this.f129c = str;
            return this;
        }

        /* renamed from: a */
        public C0033a mo113a(boolean z) {
            this.f128b = z;
            return this;
        }

        /* renamed from: b */
        public C0033a mo114b() {
            return mo112a((String) "su");
        }

        /* renamed from: b */
        public C0033a mo115b(C0071a aVar) {
            this.f136j = aVar;
            return this;
        }

        @Deprecated
        /* renamed from: b */
        public C0033a mo116b(boolean z) {
            this.f132f = z;
            return this;
        }

        @Deprecated
        /* renamed from: c */
        public C0033a mo117c(boolean z) {
            this.f131e = z;
            return this;
        }

        /* renamed from: c */
        public C0035c mo118c() {
            return new C0035c(this, null);
        }

        @Deprecated
        /* renamed from: d */
        public C0033a mo119d(boolean z) {
            this.f130d = z;
            return this;
        }

        /* renamed from: e */
        public C0033a mo120e(boolean z) {
            C0029a.m101a(6, !z);
            return this;
        }
    }

    /* renamed from: eu.chainfire.b.c$b */
    private static class C0034b {

        /* renamed from: a */
        private static int f138a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public final String[] f139b;
        /* access modifiers changed from: private */

        /* renamed from: c */
        public final int f140c;
        /* access modifiers changed from: private */

        /* renamed from: d */
        public final C0051i f141d;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public final C0052j f142e;
        /* access modifiers changed from: private */

        /* renamed from: f */
        public final C0048f f143f;
        /* access modifiers changed from: private */

        /* renamed from: g */
        public final C0047e f144g;
        /* access modifiers changed from: private */

        /* renamed from: h */
        public final String f145h;
        /* access modifiers changed from: private */

        /* renamed from: i */
        public volatile C0031b f146i = null;

        /* JADX WARNING: type inference failed for: r8v6 */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public C0034b(java.lang.Object r7, int r8, p002eu.chainfire.p005b.C0032c.C0054l r9) {
            /*
                r6 = this;
                r6.<init>()
                r0 = 0
                r6.f146i = r0
                boolean r1 = r7 instanceof java.lang.String
                r2 = 0
                r3 = 1
                if (r1 == 0) goto L_0x0015
                java.lang.String[] r1 = new java.lang.String[r3]
                java.lang.String r7 = (java.lang.String) r7
                r1[r2] = r7
                r6.f139b = r1
                goto L_0x002b
            L_0x0015:
                boolean r1 = r7 instanceof java.util.List
                if (r1 == 0) goto L_0x0026
                java.util.List r7 = (java.util.List) r7
                java.lang.String[] r1 = new java.lang.String[r2]
                java.lang.Object[] r7 = r7.toArray(r1)
            L_0x0021:
                java.lang.String[] r7 = (java.lang.String[]) r7
                r6.f139b = r7
                goto L_0x002b
            L_0x0026:
                boolean r1 = r7 instanceof java.lang.String[]
                if (r1 == 0) goto L_0x009c
                goto L_0x0021
            L_0x002b:
                r6.f140c = r8
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.util.UUID r8 = java.util.UUID.randomUUID()
                java.lang.String r8 = r8.toString()
                r7.append(r8)
                java.util.Locale r8 = java.util.Locale.ENGLISH
                java.lang.String r1 = "-%08x"
                java.lang.Object[] r4 = new java.lang.Object[r3]
                int r5 = f138a
                int r5 = r5 + r3
                f138a = r5
                java.lang.Integer r3 = java.lang.Integer.valueOf(r5)
                r4[r2] = r3
                java.lang.String r8 = java.lang.String.format(r8, r1, r4)
                r7.append(r8)
                java.lang.String r7 = r7.toString()
                r6.f145h = r7
                if (r9 == 0) goto L_0x0090
                boolean r7 = r9 instanceof p002eu.chainfire.p005b.C0032c.C0047e
                if (r7 == 0) goto L_0x0068
                r7 = r9
                eu.chainfire.b.c$e r7 = (p002eu.chainfire.p005b.C0032c.C0047e) r7
                r9 = r7
                r7 = r0
                r8 = r7
                goto L_0x0093
            L_0x0068:
                boolean r7 = r9 instanceof p002eu.chainfire.p005b.C0032c.C0048f
                if (r7 == 0) goto L_0x0073
                r7 = r9
                eu.chainfire.b.c$f r7 = (p002eu.chainfire.p005b.C0032c.C0048f) r7
                r8 = r7
                r7 = r0
                r9 = r7
                goto L_0x0093
            L_0x0073:
                boolean r7 = r9 instanceof p002eu.chainfire.p005b.C0032c.C0052j
                if (r7 == 0) goto L_0x007c
                r7 = r9
                eu.chainfire.b.c$j r7 = (p002eu.chainfire.p005b.C0032c.C0052j) r7
                r8 = r0
                goto L_0x0092
            L_0x007c:
                boolean r7 = r9 instanceof p002eu.chainfire.p005b.C0032c.C0051i
                if (r7 == 0) goto L_0x0088
                r7 = r9
                eu.chainfire.b.c$i r7 = (p002eu.chainfire.p005b.C0032c.C0051i) r7
                r8 = r0
                r9 = r8
                r0 = r7
                r7 = r9
                goto L_0x0093
            L_0x0088:
                java.lang.IllegalArgumentException r7 = new java.lang.IllegalArgumentException
                java.lang.String r8 = "OnResult is not a supported callback interface"
                r7.<init>(r8)
                throw r7
            L_0x0090:
                r7 = r0
                r8 = r7
            L_0x0092:
                r9 = r8
            L_0x0093:
                r6.f141d = r0
                r6.f142e = r7
                r6.f143f = r8
                r6.f144g = r9
                return
            L_0x009c:
                java.lang.IllegalArgumentException r7 = new java.lang.IllegalArgumentException
                java.lang.String r8 = "commands parameter must be of type String, List<String> or String[]"
                r7.<init>(r8)
                throw r7
            */
            throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.p005b.C0032c.C0034b.<init>(java.lang.Object, int, eu.chainfire.b.c$l):void");
        }
    }

    /* renamed from: eu.chainfire.b.c$c */
    public static class C0035c {

        /* renamed from: A */
        private volatile boolean f147A = false;

        /* renamed from: B */
        private final Object f148B = new Object();

        /* renamed from: C */
        private final Object f149C = new Object();
        /* access modifiers changed from: private */

        /* renamed from: D */
        public final List<String> f150D = new ArrayList();
        /* access modifiers changed from: private */

        /* renamed from: E */
        public volatile int f151E = 0;
        /* access modifiers changed from: private */

        /* renamed from: F */
        public volatile String f152F = null;
        /* access modifiers changed from: private */

        /* renamed from: G */
        public volatile String f153G = null;
        /* access modifiers changed from: private */

        /* renamed from: H */
        public volatile C0034b f154H = null;
        /* access modifiers changed from: private */

        /* renamed from: I */
        public volatile List<String> f155I = null;
        /* access modifiers changed from: private */

        /* renamed from: J */
        public volatile List<String> f156J = null;

        /* renamed from: a */
        protected final Handler f157a;

        /* renamed from: b */
        protected volatile boolean f158b = true;

        /* renamed from: c */
        protected volatile int f159c = 0;

        /* renamed from: d */
        protected final Object f160d = new Object();

        /* renamed from: e */
        private final boolean f161e;
        /* access modifiers changed from: private */

        /* renamed from: f */
        public final String f162f;
        /* access modifiers changed from: private */

        /* renamed from: g */
        public boolean f163g;

        /* renamed from: h */
        private final boolean f164h;

        /* renamed from: i */
        private final List<C0034b> f165i;

        /* renamed from: j */
        private final Map<String, String> f166j;
        /* access modifiers changed from: private */

        /* renamed from: k */
        public final C0071a f167k;
        /* access modifiers changed from: private */

        /* renamed from: l */
        public final C0071a f168l;
        /* access modifiers changed from: private */

        /* renamed from: m */
        public int f169m;

        /* renamed from: n */
        private Process f170n = null;

        /* renamed from: o */
        private DataOutputStream f171o = null;
        /* access modifiers changed from: private */

        /* renamed from: p */
        public C0070d f172p = null;
        /* access modifiers changed from: private */

        /* renamed from: q */
        public C0070d f173q = null;
        /* access modifiers changed from: private */

        /* renamed from: r */
        public final Object f174r = new Object();
        /* access modifiers changed from: private */

        /* renamed from: s */
        public boolean f175s = false;
        /* access modifiers changed from: private */

        /* renamed from: t */
        public boolean f176t = false;

        /* renamed from: u */
        private ScheduledThreadPoolExecutor f177u = null;

        /* renamed from: v */
        private volatile boolean f178v = false;

        /* renamed from: w */
        private volatile boolean f179w = false;
        /* access modifiers changed from: private */

        /* renamed from: x */
        public volatile boolean f180x = false;
        /* access modifiers changed from: private */

        /* renamed from: y */
        public volatile boolean f181y = true;

        /* renamed from: z */
        private volatile int f182z;

        protected C0035c(final C0033a aVar, final C0055m mVar) {
            this.f161e = aVar.f128b;
            this.f162f = aVar.f129c;
            this.f163g = aVar.f131e;
            this.f164h = aVar.f130d;
            this.f165i = aVar.f133g;
            this.f166j = aVar.f134h;
            this.f167k = aVar.f135i;
            this.f168l = aVar.f136j;
            this.f169m = aVar.f137k;
            this.f157a = (Looper.myLooper() == null || aVar.f127a != null || !this.f161e) ? aVar.f127a : new Handler();
            if (mVar != null || aVar.f132f) {
                this.f179w = true;
                this.f180x = true;
                this.f169m = 60;
                this.f165i.add(0, new C0034b(C0032c.f125a, 0, new C0052j() {
                    /* renamed from: a */
                    public void mo136a(int i, final int i2, List<String> list, List<String> list2) {
                        boolean z = true;
                        if (i2 == 0 && !C0032c.m118a(list, C0063q.m244b(C0035c.this.f162f))) {
                            i2 = -4;
                            C0035c.this.f181y = true;
                            C0035c.this.mo129d();
                        }
                        C0035c.this.f169m = aVar.f137k;
                        if (mVar == null) {
                            return;
                        }
                        if (C0035c.this.f157a != null) {
                            C0035c.this.mo122a();
                            C0035c.this.f157a.post(new Runnable() {
                                public void run() {
                                    try {
                                        mVar.mo149a(i2 == 0, i2);
                                    } finally {
                                        C0035c.this.mo126b();
                                    }
                                }
                            });
                            return;
                        }
                        C0055m mVar = mVar;
                        if (i2 != 0) {
                            z = false;
                        }
                        mVar.mo149a(z, i2);
                    }
                }));
            }
            if (!m192o() && mVar != null) {
                if (this.f157a != null) {
                    mo122a();
                    this.f157a.post(new Runnable() {
                        public void run() {
                            try {
                                mVar.mo149a(false, -3);
                            } finally {
                                C0035c.this.mo126b();
                            }
                        }
                    });
                    return;
                }
                mVar.mo149a(false, -3);
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public synchronized void m162a(final String str, final Object obj, final boolean z) {
            if (obj != null) {
                if (this.f157a != null) {
                    mo122a();
                    this.f157a.post(new Runnable() {
                        public void run() {
                            try {
                                if (obj instanceof C0071a) {
                                    ((C0071a) obj).mo143a(str);
                                } else if ((obj instanceof C0050h) && !z) {
                                    ((C0050h) obj).mo146a_(str);
                                } else if ((obj instanceof C0049g) && z) {
                                    ((C0049g) obj).mo145a(str);
                                }
                            } finally {
                                C0035c.this.mo126b();
                            }
                        }
                    });
                } else if (obj instanceof C0071a) {
                    ((C0071a) obj).mo143a(str);
                } else if ((obj instanceof C0050h) && !z) {
                    ((C0050h) obj).mo146a_(str);
                } else if ((obj instanceof C0049g) && z) {
                    ((C0049g) obj).mo145a(str);
                }
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public synchronized void m163a(String str, boolean z) {
            List<String> list;
            if (z) {
                try {
                    if (this.f156J != null) {
                        list = this.f156J;
                    } else if (this.f164h && this.f155I != null) {
                        list = this.f155I;
                    }
                } finally {
                }
            } else if (this.f155I != null) {
                list = this.f155I;
            }
            list.add(str);
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public boolean m164a(C0034b bVar, int i, List<String> list, List<String> list2, InputStream inputStream) {
            if (bVar.f141d == null && bVar.f142e == null && bVar.f143f == null && bVar.f144g == null) {
                return true;
            }
            if (this.f157a == null || bVar.f139b == C0032c.f125a) {
                if (inputStream == null) {
                    if (bVar.f141d != null) {
                        bVar.f141d.mo147a(bVar.f140c, i, list != null ? list : this.f150D);
                    }
                    if (bVar.f142e != null) {
                        C0052j c = bVar.f142e;
                        int h = bVar.f140c;
                        if (list == null) {
                            list = this.f150D;
                        }
                        if (list2 == null) {
                            list2 = this.f150D;
                        }
                        c.mo136a(h, i, list, list2);
                    }
                    if (bVar.f143f != null) {
                        bVar.f143f.mo148a(bVar.f140c, i);
                    }
                    if (bVar.f144g != null) {
                        bVar.f144g.mo148a(bVar.f140c, i);
                    }
                } else if (bVar.f144g != null) {
                    bVar.f144g.mo144a(inputStream);
                }
                return true;
            }
            mo122a();
            Handler handler = this.f157a;
            final InputStream inputStream2 = inputStream;
            final C0034b bVar2 = bVar;
            final int i2 = i;
            final List<String> list3 = list;
            final List<String> list4 = list2;
            C00415 r1 = new Runnable() {
                public void run() {
                    try {
                        if (inputStream2 == null) {
                            if (bVar2.f141d != null) {
                                bVar2.f141d.mo147a(bVar2.f140c, i2, list3 != null ? list3 : C0035c.this.f150D);
                            }
                            if (bVar2.f142e != null) {
                                bVar2.f142e.mo136a(bVar2.f140c, i2, list3 != null ? list3 : C0035c.this.f150D, list4 != null ? list4 : C0035c.this.f150D);
                            }
                            if (bVar2.f143f != null) {
                                bVar2.f143f.mo148a(bVar2.f140c, i2);
                            }
                            if (bVar2.f144g != null) {
                                bVar2.f144g.mo148a(bVar2.f140c, i2);
                            }
                        } else if (bVar2.f144g != null) {
                            bVar2.f144g.mo144a(inputStream2);
                        }
                    } finally {
                        C0035c.this.mo126b();
                    }
                }
            };
            handler.post(r1);
            return false;
        }

        /* renamed from: b */
        private void m170b(boolean z) {
            String[] a;
            boolean g = mo133g();
            if (!g || this.f158b) {
                this.f181y = true;
                this.f180x = false;
            }
            if (g && !this.f158b && this.f181y && this.f165i.size() > 0) {
                C0034b bVar = this.f165i.get(0);
                this.f165i.remove(0);
                this.f155I = null;
                this.f156J = null;
                this.f151E = 0;
                this.f152F = null;
                this.f153G = null;
                if (bVar.f139b.length <= 0) {
                    m170b(false);
                } else if (!(this.f171o == null || this.f172p == null)) {
                    try {
                        if (bVar.f141d != null) {
                            this.f155I = Collections.synchronizedList(new ArrayList());
                        } else if (bVar.f142e != null) {
                            this.f155I = Collections.synchronizedList(new ArrayList());
                            this.f156J = Collections.synchronizedList(new ArrayList());
                        }
                        this.f181y = false;
                        this.f154H = bVar;
                        if (bVar.f144g == null) {
                            this.f172p.mo158a();
                            m186l();
                        } else if (!this.f172p.mo161d()) {
                            if (Thread.currentThread().getId() == this.f172p.getId()) {
                                this.f172p.mo159b();
                            } else {
                                this.f171o.write("echo inputstream\n".getBytes("UTF-8"));
                                this.f171o.flush();
                                this.f172p.mo160c();
                            }
                        }
                        for (String str : bVar.f139b) {
                            C0029a.m105b(String.format(Locale.ENGLISH, "[%s+] %s", new Object[]{this.f162f.toUpperCase(Locale.ENGLISH), str}));
                            DataOutputStream dataOutputStream = this.f171o;
                            StringBuilder sb = new StringBuilder();
                            sb.append(str);
                            sb.append("\n");
                            dataOutputStream.write(sb.toString().getBytes("UTF-8"));
                        }
                        DataOutputStream dataOutputStream2 = this.f171o;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("echo ");
                        sb2.append(bVar.f145h);
                        sb2.append(" $?\n");
                        dataOutputStream2.write(sb2.toString().getBytes("UTF-8"));
                        DataOutputStream dataOutputStream3 = this.f171o;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("echo ");
                        sb3.append(bVar.f145h);
                        sb3.append(" >&2\n");
                        dataOutputStream3.write(sb3.toString().getBytes("UTF-8"));
                        this.f171o.flush();
                        if (bVar.f144g != null) {
                            bVar.f146i = new C0031b(this.f172p, bVar.f145h);
                            m164a(bVar, 0, null, null, bVar.f146i);
                        }
                    } catch (IOException unused) {
                    }
                }
            } else if (!g || this.f158b) {
                C0029a.m102a(String.format(Locale.ENGLISH, "[%s%%] SHELL_DIED", new Object[]{this.f162f.toUpperCase(Locale.ENGLISH)}));
                while (this.f165i.size() > 0) {
                    m164a(this.f165i.remove(0), -2, null, null, null);
                }
                mo127c();
            }
            if (this.f181y) {
                if (g && this.f147A) {
                    this.f147A = false;
                    mo124a(true);
                }
                if (z) {
                    synchronized (this.f148B) {
                        this.f148B.notifyAll();
                    }
                }
            }
            if (this.f179w && !this.f180x) {
                this.f179w = this.f180x;
                synchronized (this.f149C) {
                    this.f149C.notifyAll();
                }
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: j */
        public void mo156j() {
            m170b(true);
        }

        /* access modifiers changed from: private */
        /* renamed from: k */
        public synchronized void m183k() {
            int i;
            if (this.f177u != null) {
                if (this.f169m != 0) {
                    if (!mo133g()) {
                        C0029a.m102a(String.format(Locale.ENGLISH, "[%s%%] SHELL_DIED", new Object[]{this.f162f.toUpperCase(Locale.ENGLISH)}));
                        i = -2;
                    } else {
                        int i2 = this.f182z;
                        this.f182z = i2 + 1;
                        if (i2 >= this.f169m) {
                            C0029a.m102a(String.format(Locale.ENGLISH, "[%s%%] WATCHDOG_EXIT", new Object[]{this.f162f.toUpperCase(Locale.ENGLISH)}));
                            i = -1;
                        } else {
                            return;
                        }
                    }
                    if (this.f154H != null) {
                        m164a(this.f154H, i, this.f155I, this.f156J, null);
                    }
                    this.f154H = null;
                    this.f155I = null;
                    this.f156J = null;
                    this.f181y = true;
                    this.f180x = false;
                    this.f177u.shutdown();
                    this.f177u = null;
                    mo131f();
                }
            }
        }

        /* renamed from: l */
        private void m186l() {
            if (this.f169m != 0) {
                this.f182z = 0;
                this.f177u = new ScheduledThreadPoolExecutor(1);
                this.f177u.scheduleAtFixedRate(new Runnable() {
                    public void run() {
                        C0035c.this.m183k();
                    }
                }, 1, 1, TimeUnit.SECONDS);
            }
        }

        /* renamed from: m */
        private void m188m() {
            if (this.f177u != null) {
                this.f177u.shutdownNow();
                this.f177u = null;
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: n */
        public synchronized void m189n() {
            if (this.f154H != null && this.f154H.f145h.equals(this.f152F) && this.f154H.f145h.equals(this.f153G)) {
                m164a(this.f154H, this.f151E, this.f155I, this.f156J, null);
                m188m();
                this.f154H = null;
                this.f155I = null;
                this.f156J = null;
                this.f181y = true;
                this.f180x = false;
                mo156j();
            }
        }

        /* renamed from: o */
        private synchronized boolean m192o() {
            Process exec;
            C0029a.m102a(String.format(Locale.ENGLISH, "[%s%%] START", new Object[]{this.f162f.toUpperCase(Locale.ENGLISH)}));
            try {
                if (this.f166j.size() == 0) {
                    exec = Runtime.getRuntime().exec(this.f162f);
                } else {
                    HashMap hashMap = new HashMap();
                    hashMap.putAll(System.getenv());
                    hashMap.putAll(this.f166j);
                    String[] strArr = new String[hashMap.size()];
                    int i = 0;
                    for (Entry entry : hashMap.entrySet()) {
                        StringBuilder sb = new StringBuilder();
                        sb.append((String) entry.getKey());
                        sb.append("=");
                        sb.append((String) entry.getValue());
                        strArr[i] = sb.toString();
                        i++;
                    }
                    exec = Runtime.getRuntime().exec(this.f162f, strArr);
                }
                this.f170n = exec;
                if (this.f170n != null) {
                    C00426 r0 = new C0072b() {
                        /* renamed from: a */
                        public void mo142a() {
                            boolean z;
                            if (C0035c.this.f163g || !C0035c.this.mo133g()) {
                                if (C0035c.this.f173q != null && Thread.currentThread() == C0035c.this.f172p) {
                                    C0035c.this.f173q.mo158a();
                                }
                                if (C0035c.this.f172p != null && Thread.currentThread() == C0035c.this.f173q) {
                                    C0035c.this.f172p.mo158a();
                                }
                                synchronized (C0035c.this.f174r) {
                                    if (Thread.currentThread() == C0035c.this.f172p) {
                                        C0035c.this.f175s = true;
                                    }
                                    if (Thread.currentThread() == C0035c.this.f173q) {
                                        C0035c.this.f176t = true;
                                    }
                                    z = C0035c.this.f175s && C0035c.this.f176t;
                                    C0034b j = C0035c.this.f154H;
                                    if (j != null) {
                                        C0031b f = j.f146i;
                                        if (f != null) {
                                            f.mo101b();
                                        }
                                    }
                                }
                                if (z) {
                                    C0035c.this.m194p();
                                    synchronized (C0035c.this) {
                                        if (C0035c.this.f154H != null) {
                                            C0035c.this.m164a(C0035c.this.f154H, -2, C0035c.this.f155I, C0035c.this.f156J, null);
                                            C0035c.this.f154H = null;
                                        }
                                        C0035c.this.f158b = true;
                                        C0035c.this.f180x = false;
                                        C0035c.this.mo156j();
                                    }
                                }
                            }
                        }
                    };
                    this.f171o = new DataOutputStream(this.f170n.getOutputStream());
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(this.f162f.toUpperCase(Locale.ENGLISH));
                    sb2.append("-");
                    this.f172p = new C0070d(sb2.toString(), this.f170n.getInputStream(), new C0071a() {
                        /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b6, code lost:
                            return;
                         */
                        /* renamed from: a */
                        /* Code decompiled incorrectly, please refer to instructions dump. */
                        public void mo143a(java.lang.String r7) {
                            /*
                                r6 = this;
                                eu.chainfire.b.c$c r0 = p002eu.chainfire.p005b.C0032c.C0035c.this
                                eu.chainfire.b.c$b r0 = r0.f154H
                                if (r0 == 0) goto L_0x0028
                                eu.chainfire.b.c$e r0 = r0.f144g
                                if (r0 == 0) goto L_0x0028
                                java.lang.String r0 = "inputstream"
                                boolean r0 = r7.equals(r0)
                                if (r0 == 0) goto L_0x0028
                                eu.chainfire.b.c$c r7 = p002eu.chainfire.p005b.C0032c.C0035c.this
                                eu.chainfire.b.d r7 = r7.f172p
                                if (r7 == 0) goto L_0x0027
                                eu.chainfire.b.c$c r7 = p002eu.chainfire.p005b.C0032c.C0035c.this
                                eu.chainfire.b.d r7 = r7.f172p
                                r7.mo159b()
                            L_0x0027:
                                return
                            L_0x0028:
                                eu.chainfire.b.c$c r0 = p002eu.chainfire.p005b.C0032c.C0035c.this
                                monitor-enter(r0)
                                eu.chainfire.b.c$c r1 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x00b7 }
                                eu.chainfire.b.c$b r1 = r1.f154H     // Catch:{ all -> 0x00b7 }
                                if (r1 != 0) goto L_0x0035
                                monitor-exit(r0)     // Catch:{ all -> 0x00b7 }
                                return
                            L_0x0035:
                                eu.chainfire.b.c$c r1 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x00b7 }
                                eu.chainfire.b.c$b r1 = r1.f154H     // Catch:{ all -> 0x00b7 }
                                java.lang.String r1 = r1.f145h     // Catch:{ all -> 0x00b7 }
                                int r1 = r7.indexOf(r1)     // Catch:{ all -> 0x00b7 }
                                r2 = 0
                                r3 = 0
                                if (r1 != 0) goto L_0x0048
                                goto L_0x0056
                            L_0x0048:
                                if (r1 <= 0) goto L_0x0053
                                java.lang.String r2 = r7.substring(r3, r1)     // Catch:{ all -> 0x00b7 }
                                java.lang.String r7 = r7.substring(r1)     // Catch:{ all -> 0x00b7 }
                                goto L_0x0056
                            L_0x0053:
                                r5 = r2
                                r2 = r7
                                r7 = r5
                            L_0x0056:
                                if (r2 == 0) goto L_0x0077
                                eu.chainfire.b.c$c r1 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x00b7 }
                                r1.m163a(r2, r3)     // Catch:{ all -> 0x00b7 }
                                eu.chainfire.b.c$c r1 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x00b7 }
                                eu.chainfire.b.c$c r4 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x00b7 }
                                eu.chainfire.b.d$a r4 = r4.f167k     // Catch:{ all -> 0x00b7 }
                                r1.m162a(r2, r4, r3)     // Catch:{ all -> 0x00b7 }
                                eu.chainfire.b.c$c r1 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x00b7 }
                                eu.chainfire.b.c$c r4 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x00b7 }
                                eu.chainfire.b.c$b r4 = r4.f154H     // Catch:{ all -> 0x00b7 }
                                eu.chainfire.b.c$f r4 = r4.f143f     // Catch:{ all -> 0x00b7 }
                                r1.m162a(r2, r4, r3)     // Catch:{ all -> 0x00b7 }
                            L_0x0077:
                                if (r7 == 0) goto L_0x00b5
                                eu.chainfire.b.c$c r1 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ Exception -> 0x009d }
                                eu.chainfire.b.c$c r2 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ Exception -> 0x009d }
                                eu.chainfire.b.c$b r2 = r2.f154H     // Catch:{ Exception -> 0x009d }
                                java.lang.String r2 = r2.f145h     // Catch:{ Exception -> 0x009d }
                                int r2 = r2.length()     // Catch:{ Exception -> 0x009d }
                                int r2 = r2 + 1
                                java.lang.String r7 = r7.substring(r2)     // Catch:{ Exception -> 0x009d }
                                r2 = 10
                                java.lang.Integer r7 = java.lang.Integer.valueOf(r7, r2)     // Catch:{ Exception -> 0x009d }
                                int r7 = r7.intValue()     // Catch:{ Exception -> 0x009d }
                                r1.f151E = r7     // Catch:{ Exception -> 0x009d }
                                goto L_0x00a1
                            L_0x009d:
                                r7 = move-exception
                                r7.printStackTrace()     // Catch:{ all -> 0x00b7 }
                            L_0x00a1:
                                eu.chainfire.b.c$c r7 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x00b7 }
                                eu.chainfire.b.c$c r1 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x00b7 }
                                eu.chainfire.b.c$b r1 = r1.f154H     // Catch:{ all -> 0x00b7 }
                                java.lang.String r1 = r1.f145h     // Catch:{ all -> 0x00b7 }
                                r7.f152F = r1     // Catch:{ all -> 0x00b7 }
                                eu.chainfire.b.c$c r7 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x00b7 }
                                r7.m189n()     // Catch:{ all -> 0x00b7 }
                            L_0x00b5:
                                monitor-exit(r0)     // Catch:{ all -> 0x00b7 }
                                return
                            L_0x00b7:
                                r7 = move-exception
                                monitor-exit(r0)     // Catch:{ all -> 0x00b7 }
                                throw r7
                            */
                            throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.p005b.C0032c.C0035c.C00437.mo143a(java.lang.String):void");
                        }
                    }, r0);
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(this.f162f.toUpperCase(Locale.ENGLISH));
                    sb3.append("*");
                    this.f173q = new C0070d(sb3.toString(), this.f170n.getErrorStream(), new C0071a() {
                        /* JADX WARNING: Code restructure failed: missing block: B:17:0x006e, code lost:
                            return;
                         */
                        /* renamed from: a */
                        /* Code decompiled incorrectly, please refer to instructions dump. */
                        public void mo143a(java.lang.String r6) {
                            /*
                                r5 = this;
                                eu.chainfire.b.c$c r0 = p002eu.chainfire.p005b.C0032c.C0035c.this
                                monitor-enter(r0)
                                eu.chainfire.b.c$c r1 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x006f }
                                eu.chainfire.b.c$b r1 = r1.f154H     // Catch:{ all -> 0x006f }
                                if (r1 != 0) goto L_0x000d
                                monitor-exit(r0)     // Catch:{ all -> 0x006f }
                                return
                            L_0x000d:
                                eu.chainfire.b.c$c r1 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x006f }
                                eu.chainfire.b.c$b r1 = r1.f154H     // Catch:{ all -> 0x006f }
                                java.lang.String r1 = r1.f145h     // Catch:{ all -> 0x006f }
                                int r1 = r6.indexOf(r1)     // Catch:{ all -> 0x006f }
                                if (r1 != 0) goto L_0x001f
                                r6 = 0
                                goto L_0x0026
                            L_0x001f:
                                if (r1 <= 0) goto L_0x0026
                                r2 = 0
                                java.lang.String r6 = r6.substring(r2, r1)     // Catch:{ all -> 0x006f }
                            L_0x0026:
                                if (r6 == 0) goto L_0x0057
                                eu.chainfire.b.c$c r2 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x006f }
                                r3 = 1
                                r2.m163a(r6, r3)     // Catch:{ all -> 0x006f }
                                eu.chainfire.b.c$c r2 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x006f }
                                eu.chainfire.b.c$c r4 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x006f }
                                eu.chainfire.b.d$a r4 = r4.f168l     // Catch:{ all -> 0x006f }
                                r2.m162a(r6, r4, r3)     // Catch:{ all -> 0x006f }
                                eu.chainfire.b.c$c r2 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x006f }
                                eu.chainfire.b.c$c r4 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x006f }
                                eu.chainfire.b.c$b r4 = r4.f154H     // Catch:{ all -> 0x006f }
                                eu.chainfire.b.c$f r4 = r4.f143f     // Catch:{ all -> 0x006f }
                                r2.m162a(r6, r4, r3)     // Catch:{ all -> 0x006f }
                                eu.chainfire.b.c$c r2 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x006f }
                                eu.chainfire.b.c$c r4 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x006f }
                                eu.chainfire.b.c$b r4 = r4.f154H     // Catch:{ all -> 0x006f }
                                eu.chainfire.b.c$e r4 = r4.f144g     // Catch:{ all -> 0x006f }
                                r2.m162a(r6, r4, r3)     // Catch:{ all -> 0x006f }
                            L_0x0057:
                                if (r1 < 0) goto L_0x006d
                                eu.chainfire.b.c$c r6 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x006f }
                                eu.chainfire.b.c$c r1 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x006f }
                                eu.chainfire.b.c$b r1 = r1.f154H     // Catch:{ all -> 0x006f }
                                java.lang.String r1 = r1.f145h     // Catch:{ all -> 0x006f }
                                r6.f153G = r1     // Catch:{ all -> 0x006f }
                                eu.chainfire.b.c$c r6 = p002eu.chainfire.p005b.C0032c.C0035c.this     // Catch:{ all -> 0x006f }
                                r6.m189n()     // Catch:{ all -> 0x006f }
                            L_0x006d:
                                monitor-exit(r0)     // Catch:{ all -> 0x006f }
                                return
                            L_0x006f:
                                r6 = move-exception
                                monitor-exit(r0)     // Catch:{ all -> 0x006f }
                                throw r6
                            */
                            throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.p005b.C0032c.C0035c.C00448.mo143a(java.lang.String):void");
                        }
                    }, r0);
                    this.f172p.start();
                    this.f173q.start();
                    this.f178v = true;
                    this.f158b = false;
                    mo156j();
                } else {
                    throw new NullPointerException();
                }
            } catch (IOException unused) {
                return false;
            }
            return true;
        }

        /* access modifiers changed from: private */
        /* renamed from: p */
        public boolean m194p() {
            if (!(this.f157a == null || this.f157a.getLooper() == null || this.f157a.getLooper() == Looper.myLooper())) {
                synchronized (this.f160d) {
                    while (this.f159c > 0) {
                        try {
                            this.f160d.wait();
                        } catch (InterruptedException unused) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        /* renamed from: a */
        public int mo121a(Object obj, final List<String> list, final List<String> list2, boolean z) {
            if (z) {
                if (list != null) {
                    list.clear();
                }
                if (list2 != null) {
                    list2.clear();
                }
            }
            final int[] iArr = new int[1];
            mo123a(obj, 0, (C0054l) new C0052j() {
                /* renamed from: a */
                public void mo136a(int i, int i2, List<String> list, List<String> list2) {
                    iArr[0] = i2;
                    if (list != null) {
                        list.addAll(list);
                    }
                    if (list2 != null) {
                        list2.addAll(list2);
                    }
                }
            });
            mo135i();
            if (iArr[0] >= 0) {
                return iArr[0];
            }
            throw new C0064r();
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo122a() {
            synchronized (this.f160d) {
                this.f159c++;
            }
        }

        /* renamed from: a */
        public synchronized void mo123a(Object obj, int i, C0054l lVar) {
            this.f165i.add(new C0034b(obj, i, lVar));
            mo156j();
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Can't wrap try/catch for region: R(10:43|44|(1:46)|47|(1:49)|50|(1:54)|55|56|58) */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0026, code lost:
            if (mo133g() != false) goto L_0x002c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0028, code lost:
            mo127c();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x002b, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x002c, code lost:
            if (r6 != false) goto L_0x0048;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0032, code lost:
            if (p002eu.chainfire.p005b.C0029a.m109c() == false) goto L_0x0048;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0038, code lost:
            if (p002eu.chainfire.p005b.C0029a.m111d() != false) goto L_0x003b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x003b, code lost:
            p002eu.chainfire.p005b.C0029a.m102a((java.lang.String) "Application attempted to wait for a non-idle shell to close on the main thread");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0047, code lost:
            throw new p002eu.chainfire.p005b.C0032c.C0066t("Application attempted to wait for a non-idle shell to close on the main thread");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0048, code lost:
            if (r6 != false) goto L_0x004d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x004a, code lost:
            mo135i();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
            r5.f171o.write("exit\n".getBytes("UTF-8"));
            r5.f171o.flush();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0060, code lost:
            r6 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x006b, code lost:
            if (r6.getMessage().contains("EPIPE") == false) goto L_0x006d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x0077, code lost:
            if (r6.getMessage().contains("Stream closed") != false) goto L_0x0079;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x007a, code lost:
            throw r6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x007b, code lost:
            r5.f170n.waitFor();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
            r5.f171o.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x008b, code lost:
            if (java.lang.Thread.currentThread() != r5.f172p) goto L_0x008d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x008d, code lost:
            r5.f172p.mo158a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x0098, code lost:
            if (java.lang.Thread.currentThread() != r5.f173q) goto L_0x009a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:0x009a, code lost:
            r5.f173q.mo158a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:0x00af, code lost:
            r5.f172p.join();
            r5.f173q.join();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x00b9, code lost:
            m188m();
            r5.f170n.destroy();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x00c1, code lost:
            p002eu.chainfire.p005b.C0029a.m102a(java.lang.String.format(java.util.Locale.ENGLISH, "[%s%%] END", new java.lang.Object[]{r5.f162f.toUpperCase(java.util.Locale.ENGLISH)}));
            mo127c();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x00db, code lost:
            return;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x0085 */
        /* JADX WARNING: Removed duplicated region for block: B:57:? A[ExcHandler: InterruptedException (unused java.lang.InterruptedException), SYNTHETIC, Splitter:B:33:0x0061] */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void mo124a(boolean r6) {
            /*
                r5 = this;
                java.io.DataOutputStream r6 = r5.f171o
                if (r6 == 0) goto L_0x00df
                eu.chainfire.b.d r6 = r5.f172p
                if (r6 == 0) goto L_0x00df
                eu.chainfire.b.d r6 = r5.f173q
                if (r6 == 0) goto L_0x00df
                java.lang.Process r6 = r5.f170n
                if (r6 == 0) goto L_0x00df
                boolean r6 = r5.mo134h()
                monitor-enter(r5)
                boolean r0 = r5.f178v     // Catch:{ all -> 0x00dc }
                if (r0 != 0) goto L_0x001b
                monitor-exit(r5)     // Catch:{ all -> 0x00dc }
                return
            L_0x001b:
                r0 = 0
                r5.f178v = r0     // Catch:{ all -> 0x00dc }
                r1 = 1
                r5.f158b = r1     // Catch:{ all -> 0x00dc }
                monitor-exit(r5)     // Catch:{ all -> 0x00dc }
                boolean r2 = r5.mo133g()
                if (r2 != 0) goto L_0x002c
                r5.mo127c()
                return
            L_0x002c:
                if (r6 != 0) goto L_0x0048
                boolean r2 = p002eu.chainfire.p005b.C0029a.m109c()
                if (r2 == 0) goto L_0x0048
                boolean r2 = p002eu.chainfire.p005b.C0029a.m111d()
                if (r2 != 0) goto L_0x003b
                goto L_0x0048
            L_0x003b:
                java.lang.String r6 = "Application attempted to wait for a non-idle shell to close on the main thread"
                p002eu.chainfire.p005b.C0029a.m102a(r6)
                eu.chainfire.b.c$t r6 = new eu.chainfire.b.c$t
                java.lang.String r0 = "Application attempted to wait for a non-idle shell to close on the main thread"
                r6.<init>(r0)
                throw r6
            L_0x0048:
                if (r6 != 0) goto L_0x004d
                r5.mo135i()
            L_0x004d:
                java.io.DataOutputStream r6 = r5.f171o     // Catch:{ IOException -> 0x0060 }
                java.lang.String r2 = "exit\n"
                java.lang.String r3 = "UTF-8"
                byte[] r2 = r2.getBytes(r3)     // Catch:{ IOException -> 0x0060 }
                r6.write(r2)     // Catch:{ IOException -> 0x0060 }
                java.io.DataOutputStream r6 = r5.f171o     // Catch:{ IOException -> 0x0060 }
                r6.flush()     // Catch:{ IOException -> 0x0060 }
                goto L_0x007b
            L_0x0060:
                r6 = move-exception
                java.lang.String r2 = r6.getMessage()     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                java.lang.String r3 = "EPIPE"
                boolean r2 = r2.contains(r3)     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                if (r2 != 0) goto L_0x007b
                java.lang.String r2 = r6.getMessage()     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                java.lang.String r3 = "Stream closed"
                boolean r2 = r2.contains(r3)     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                if (r2 == 0) goto L_0x007a
                goto L_0x007b
            L_0x007a:
                throw r6     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
            L_0x007b:
                java.lang.Process r6 = r5.f170n     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                r6.waitFor()     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                java.io.DataOutputStream r6 = r5.f171o     // Catch:{ IOException -> 0x0085 }
                r6.close()     // Catch:{ IOException -> 0x0085 }
            L_0x0085:
                java.lang.Thread r6 = java.lang.Thread.currentThread()     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                eu.chainfire.b.d r2 = r5.f172p     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                if (r6 == r2) goto L_0x0092
                eu.chainfire.b.d r6 = r5.f172p     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                r6.mo158a()     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
            L_0x0092:
                java.lang.Thread r6 = java.lang.Thread.currentThread()     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                eu.chainfire.b.d r2 = r5.f173q     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                if (r6 == r2) goto L_0x009f
                eu.chainfire.b.d r6 = r5.f173q     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                r6.mo158a()     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
            L_0x009f:
                java.lang.Thread r6 = java.lang.Thread.currentThread()     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                eu.chainfire.b.d r2 = r5.f172p     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                if (r6 == r2) goto L_0x00b9
                java.lang.Thread r6 = java.lang.Thread.currentThread()     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                eu.chainfire.b.d r2 = r5.f173q     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                if (r6 == r2) goto L_0x00b9
                eu.chainfire.b.d r6 = r5.f172p     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                r6.join()     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                eu.chainfire.b.d r6 = r5.f173q     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                r6.join()     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
            L_0x00b9:
                r5.m188m()     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                java.lang.Process r6 = r5.f170n     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
                r6.destroy()     // Catch:{ InterruptedException -> 0x00c1, InterruptedException -> 0x00c1 }
            L_0x00c1:
                java.util.Locale r6 = java.util.Locale.ENGLISH
                java.lang.String r2 = "[%s%%] END"
                java.lang.Object[] r1 = new java.lang.Object[r1]
                java.lang.String r3 = r5.f162f
                java.util.Locale r4 = java.util.Locale.ENGLISH
                java.lang.String r3 = r3.toUpperCase(r4)
                r1[r0] = r3
                java.lang.String r6 = java.lang.String.format(r6, r2, r1)
                p002eu.chainfire.p005b.C0029a.m102a(r6)
                r5.mo127c()
                return
            L_0x00dc:
                r6 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x00dc }
                throw r6
            L_0x00df:
                java.lang.NullPointerException r6 = new java.lang.NullPointerException
                r6.<init>()
                throw r6
            */
            throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.p005b.C0032c.C0035c.mo124a(boolean):void");
        }

        /* renamed from: a */
        public boolean mo125a(Boolean bool) {
            if (!C0029a.m109c() || !C0029a.m111d()) {
                if (mo133g()) {
                    synchronized (this.f149C) {
                        while (this.f180x) {
                            try {
                                this.f149C.wait();
                            } catch (InterruptedException unused) {
                                if (bool != null) {
                                    return bool.booleanValue();
                                }
                            }
                        }
                    }
                }
                return mo133g();
            }
            C0029a.m102a((String) "Application attempted to wait for a shell to become idle on the main thread");
            throw new C0066t("Application attempted to wait for a shell to become idle on the main thread");
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void mo126b() {
            synchronized (this.f160d) {
                this.f159c--;
                if (this.f159c == 0) {
                    this.f160d.notifyAll();
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void mo127c() {
        }

        public void close() {
            mo129d();
        }

        /* renamed from: d */
        public void mo129d() {
            mo124a(false);
        }

        /* renamed from: e */
        public void mo130e() {
            if (this.f181y) {
                mo124a(true);
            } else {
                this.f147A = true;
            }
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(13:6|7|8|9|10|11|12|1f|17|18|(3:22|34|27)|32|33) */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0019 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0014 */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0020 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0035 A[SYNTHETIC] */
        /* renamed from: f */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public synchronized void mo131f() {
            /*
                r3 = this;
                monitor-enter(r3)
                java.io.DataOutputStream r0 = r3.f171o     // Catch:{ all -> 0x004d }
                if (r0 == 0) goto L_0x0047
                java.lang.Process r0 = r3.f170n     // Catch:{ all -> 0x004d }
                if (r0 == 0) goto L_0x0047
                r0 = 0
                r3.f178v = r0     // Catch:{ all -> 0x004d }
                r1 = 1
                r3.f158b = r1     // Catch:{ all -> 0x004d }
                java.io.DataOutputStream r2 = r3.f171o     // Catch:{ IOException -> 0x0014 }
                r2.close()     // Catch:{ IOException -> 0x0014 }
            L_0x0014:
                java.lang.Process r2 = r3.f170n     // Catch:{ Exception -> 0x0019 }
                r2.destroy()     // Catch:{ Exception -> 0x0019 }
            L_0x0019:
                r3.f181y = r1     // Catch:{ all -> 0x004d }
                r3.f180x = r0     // Catch:{ all -> 0x004d }
                java.lang.Object r0 = r3.f148B     // Catch:{ all -> 0x004d }
                monitor-enter(r0)     // Catch:{ all -> 0x004d }
                java.lang.Object r1 = r3.f148B     // Catch:{ all -> 0x0044 }
                r1.notifyAll()     // Catch:{ all -> 0x0044 }
                monitor-exit(r0)     // Catch:{ all -> 0x0044 }
                boolean r0 = r3.f179w     // Catch:{ all -> 0x004d }
                if (r0 == 0) goto L_0x003f
                boolean r0 = r3.f180x     // Catch:{ all -> 0x004d }
                if (r0 != 0) goto L_0x003f
                boolean r0 = r3.f180x     // Catch:{ all -> 0x004d }
                r3.f179w = r0     // Catch:{ all -> 0x004d }
                java.lang.Object r0 = r3.f149C     // Catch:{ all -> 0x004d }
                monitor-enter(r0)     // Catch:{ all -> 0x004d }
                java.lang.Object r1 = r3.f149C     // Catch:{ all -> 0x003c }
                r1.notifyAll()     // Catch:{ all -> 0x003c }
                monitor-exit(r0)     // Catch:{ all -> 0x003c }
                goto L_0x003f
            L_0x003c:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x003c }
                throw r1     // Catch:{ all -> 0x004d }
            L_0x003f:
                r3.mo127c()     // Catch:{ all -> 0x004d }
                monitor-exit(r3)
                return
            L_0x0044:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0044 }
                throw r1     // Catch:{ all -> 0x004d }
            L_0x0047:
                java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch:{ all -> 0x004d }
                r0.<init>()     // Catch:{ all -> 0x004d }
                throw r0     // Catch:{ all -> 0x004d }
            L_0x004d:
                r0 = move-exception
                monitor-exit(r3)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.p005b.C0032c.C0035c.mo131f():void");
        }

        /* access modifiers changed from: protected */
        public void finalize() {
            if (this.f158b || !C0029a.m109c()) {
                super.finalize();
            } else {
                C0029a.m102a((String) "Application did not close() interactive shell");
                throw new C0065s();
            }
        }

        /* renamed from: g */
        public boolean mo133g() {
            if (this.f170n == null) {
                return false;
            }
            try {
                this.f170n.exitValue();
                return false;
            } catch (IllegalThreadStateException unused) {
                return true;
            }
        }

        /* renamed from: h */
        public synchronized boolean mo134h() {
            if (!mo133g()) {
                this.f181y = true;
                this.f180x = false;
                synchronized (this.f148B) {
                    this.f148B.notifyAll();
                }
                if (this.f179w && !this.f180x) {
                    this.f179w = this.f180x;
                    synchronized (this.f149C) {
                        this.f149C.notifyAll();
                    }
                }
            }
            return this.f181y;
        }

        /* renamed from: i */
        public boolean mo135i() {
            if (C0029a.m109c() && C0029a.m111d()) {
                C0029a.m102a((String) "Application attempted to wait for a shell to become idle on the main thread");
                throw new C0066t("Application attempted to wait for a shell to become idle on the main thread");
            } else if (!mo133g()) {
                return true;
            } else {
                synchronized (this.f148B) {
                    while (!this.f181y) {
                        try {
                            this.f148B.wait();
                        } catch (InterruptedException unused) {
                            return false;
                        }
                    }
                }
                return m194p();
            }
        }
    }

    /* renamed from: eu.chainfire.b.c$d */
    public interface C0046d extends C0049g {
        /* renamed from: a */
        void mo144a(InputStream inputStream);
    }

    /* renamed from: eu.chainfire.b.c$e */
    public interface C0047e extends C0046d, C0053k {
    }

    /* renamed from: eu.chainfire.b.c$f */
    public interface C0048f extends C0049g, C0050h, C0053k {
    }

    /* renamed from: eu.chainfire.b.c$g */
    private interface C0049g {
        /* renamed from: a */
        void mo145a(String str);
    }

    /* renamed from: eu.chainfire.b.c$h */
    private interface C0050h {
        /* renamed from: a_ */
        void mo146a_(String str);
    }

    @Deprecated
    /* renamed from: eu.chainfire.b.c$i */
    public interface C0051i extends C0054l {
        /* renamed from: a */
        void mo147a(int i, int i2, List<String> list);
    }

    /* renamed from: eu.chainfire.b.c$j */
    public interface C0052j extends C0054l {
        /* renamed from: a */
        void mo136a(int i, int i2, List<String> list, List<String> list2);
    }

    /* renamed from: eu.chainfire.b.c$k */
    private interface C0053k extends C0054l {
        /* renamed from: a */
        void mo148a(int i, int i2);
    }

    /* renamed from: eu.chainfire.b.c$l */
    public interface C0054l {
    }

    /* renamed from: eu.chainfire.b.c$m */
    public interface C0055m extends C0054l {
        /* renamed from: a */
        void mo149a(boolean z, int i);
    }

    /* renamed from: eu.chainfire.b.c$n */
    public static class C0056n {

        /* renamed from: a */
        public static final C0059a f208a = new C0059a() {
            /* renamed from: a */
            public C0033a mo150a() {
                return new C0033a().mo119d(true).mo107a(0).mo120e(false);
            }
        };

        /* renamed from: b */
        public static final C0060o f209b = m227b((String) "sh");

        /* renamed from: c */
        public static final C0060o f210c = m227b((String) "su");

        /* renamed from: d */
        private static C0059a f211d = null;

        /* renamed from: e */
        private static Map<String, ArrayList<C0067u>> f212e = new HashMap();

        /* renamed from: f */
        private static int f213f = 4;

        /* renamed from: eu.chainfire.b.c$n$a */
        public interface C0059a {
            /* renamed from: a */
            C0033a mo150a();
        }

        /* renamed from: a */
        private static C0033a m221a() {
            synchronized (C0056n.class) {
                if (f211d != null) {
                    C0033a a = f211d.mo150a();
                    return a;
                }
                C0033a a2 = f208a.mo150a();
                return a2;
            }
        }

        /* renamed from: a */
        public static C0067u m222a(String str) {
            return m223a(str, (C0055m) null);
        }

        @SuppressLint({"WrongThread"})
        /* renamed from: a */
        public static C0067u m223a(String str, final C0055m mVar) {
            final C0067u uVar;
            String upperCase = str.toUpperCase(Locale.ENGLISH);
            synchronized (C0056n.class) {
                m226a((C0067u) null, false);
                ArrayList arrayList = f212e.get(upperCase);
                if (arrayList != null) {
                    Iterator it = arrayList.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        uVar = (C0067u) it.next();
                        if (!uVar.m253m()) {
                            uVar.m250c(true);
                            break;
                        }
                    }
                }
                uVar = null;
            }
            if (uVar == null) {
                uVar = m224a(str, mVar, true);
                if (!uVar.mo133g()) {
                    throw new C0064r();
                } else if ((!C0029a.m109c() || !C0029a.m111d()) && !uVar.mo125a((Boolean) null)) {
                    throw new C0064r();
                } else {
                    synchronized (C0056n.class) {
                        if (!uVar.mo156j()) {
                            if (f212e.get(upperCase) == null) {
                                f212e.put(upperCase, new ArrayList());
                            }
                            f212e.get(upperCase).add(uVar);
                        }
                    }
                }
            } else if (mVar != null) {
                uVar.mo122a();
                uVar.f157a.post(new Runnable() {
                    public void run() {
                        try {
                            mVar.mo149a(true, 0);
                        } finally {
                            uVar.mo126b();
                        }
                    }
                });
            }
            return uVar;
        }

        /* renamed from: a */
        private static C0067u m224a(String str, C0055m mVar, boolean z) {
            C0029a.m110d(String.format(Locale.ENGLISH, "newInstance(shell:%s, pooled:%d)", new Object[]{str, Integer.valueOf(z ? 1 : 0)}));
            return m221a().mo112a(str).m120a(mVar, z);
        }

        /* renamed from: a */
        private static void m226a(C0067u uVar, boolean z) {
            String[] strArr;
            for (String str : (String[]) f212e.keySet().toArray(new String[0])) {
                ArrayList arrayList = f212e.get(str);
                if (arrayList != null) {
                    int i = C0063q.m244b(str) ? f213f : 1;
                    int i2 = 0;
                    int i3 = 0;
                    for (int size = arrayList.size() - 1; size >= 0; size--) {
                        C0067u uVar2 = (C0067u) arrayList.get(size);
                        if (!uVar2.mo133g() || uVar2 == uVar || z) {
                            if (z) {
                                uVar2.mo130e();
                            }
                            C0029a.m110d("shell removed");
                            arrayList.remove(size);
                        } else {
                            i2++;
                            if (!uVar2.m253m()) {
                                i3++;
                            }
                        }
                    }
                    if (i2 > i && i3 > 1) {
                        int min = Math.min(i3 - 1, i2 - i);
                        for (int size2 = arrayList.size() - 1; size2 >= 0; size2--) {
                            C0067u uVar3 = (C0067u) arrayList.get(size2);
                            if (!uVar3.m253m() && uVar3.mo134h()) {
                                arrayList.remove(size2);
                                C0029a.m110d("shell killed");
                                uVar3.m248b(true);
                                min--;
                                if (min == 0) {
                                    break;
                                }
                            }
                        }
                    }
                    if (arrayList.size() == 0) {
                        f212e.remove(str);
                    }
                }
            }
            if (C0029a.m104a()) {
                for (String next : f212e.keySet()) {
                    ArrayList arrayList2 = f212e.get(next);
                    if (arrayList2 != null) {
                        int i4 = 0;
                        for (int i5 = 0; i5 < arrayList2.size(); i5++) {
                            if (((C0067u) arrayList2.get(i5)).m253m()) {
                                i4++;
                            }
                        }
                        C0029a.m110d(String.format(Locale.ENGLISH, "cleanup: shell:%s count:%d reserved:%d", new Object[]{next, Integer.valueOf(arrayList2.size()), Integer.valueOf(i4)}));
                    }
                }
            }
        }

        /* renamed from: b */
        public static C0060o m227b(String str) {
            return (!str.toUpperCase(Locale.ENGLISH).equals("SH") || f209b == null) ? (!str.toUpperCase(Locale.ENGLISH).equals("SU") || f210c == null) ? new C0060o(str) : f210c : f209b;
        }

        /* access modifiers changed from: private */
        /* renamed from: c */
        public static void m229c(C0067u uVar) {
            C0029a.m110d("releaseReservation");
            uVar.m250c(false);
            m226a((C0067u) null, false);
        }

        /* access modifiers changed from: private */
        /* renamed from: d */
        public static synchronized void m230d(C0067u uVar) {
            synchronized (C0056n.class) {
                C0029a.m110d("removeShell");
                m226a(uVar, false);
            }
        }
    }

    /* renamed from: eu.chainfire.b.c$o */
    public static class C0060o {

        /* renamed from: a */
        private final String f216a;

        public C0060o(String str) {
            this.f216a = str;
        }

        /* renamed from: a */
        public int mo152a(Object obj, List<String> list, List<String> list2, boolean z) {
            C0067u a = mo153a();
            try {
                return a.mo121a(obj, list, list2, z);
            } finally {
                a.close();
            }
        }

        /* renamed from: a */
        public C0067u mo153a() {
            return C0056n.m222a(this.f216a);
        }

        @Deprecated
        /* renamed from: a */
        public List<String> mo154a(Object obj, final boolean z) {
            C0067u a;
            try {
                a = mo153a();
                final int[] iArr = new int[1];
                final ArrayList arrayList = new ArrayList();
                a.mo123a(obj, 0, (C0054l) new C0052j() {
                    /* renamed from: a */
                    public void mo136a(int i, int i2, List<String> list, List<String> list2) {
                        iArr[0] = i2;
                        arrayList.addAll(list);
                        if (z) {
                            arrayList.addAll(list2);
                        }
                    }
                });
                a.mo135i();
                if (iArr[0] < 0) {
                    a.close();
                    return null;
                }
                a.close();
                return arrayList;
            } catch (C0064r unused) {
                return null;
            } catch (Throwable th) {
                a.close();
                throw th;
            }
        }

        @Deprecated
        /* renamed from: a */
        public List<String> mo155a(Object obj, String[] strArr, boolean z) {
            String[] strArr2;
            if (strArr == null) {
                return mo154a(obj, z);
            }
            if (obj instanceof String) {
                strArr2 = new String[]{(String) obj};
            } else {
                if (obj instanceof List) {
                    obj = ((List) obj).toArray(new String[0]);
                } else if (!(obj instanceof String[])) {
                    throw new IllegalArgumentException("commands parameter must be of type String, List<String> or String[]");
                }
                strArr2 = (String[]) obj;
            }
            StringBuilder sb = new StringBuilder();
            for (String str : strArr) {
                int indexOf = str.indexOf("=");
                if (indexOf >= 0) {
                    int i = indexOf + 1;
                    boolean equals = str.substring(i, indexOf + 2).equals("\"");
                    sb.append(str, 0, indexOf);
                    sb.append(equals ? "=" : "=\"");
                    sb.append(str.substring(i));
                    sb.append(equals ? " " : "\" ");
                }
            }
            sb.append("sh -c \"\n");
            for (String append : strArr2) {
                sb.append(append);
                sb.append("\n");
            }
            sb.append("\"");
            return mo154a(new String[]{sb.toString().replace("\\", "\\\\").replace("$", "\\$")}, z);
        }
    }

    /* renamed from: eu.chainfire.b.c$p */
    public static class C0062p {
        @Deprecated
        /* renamed from: a */
        public static List<String> m238a(String str) {
            return C0032c.m116a("sh", new String[]{str}, null, false);
        }

        @Deprecated
        /* renamed from: a */
        public static List<String> m239a(String[] strArr) {
            return C0032c.m116a("sh", strArr, null, false);
        }
    }

    /* renamed from: eu.chainfire.b.c$q */
    public static class C0063q {

        /* renamed from: a */
        private static Boolean f221a;

        /* renamed from: b */
        private static String[] f222b = {null, null};

        /* renamed from: a */
        public static synchronized String m240a(boolean z) {
            String str;
            List list;
            String str2;
            synchronized (C0063q.class) {
                char c = !z;
                if (f222b[c] == null) {
                    if (!C0032c.f126b) {
                        list = C0032c.m116a(z ? "su -V" : "su -v", new String[]{"exit"}, null, false);
                    } else {
                        list = new ArrayList();
                        try {
                            List arrayList = new ArrayList();
                            try {
                                C0060o oVar = C0056n.f209b;
                                String[] strArr = new String[2];
                                strArr[0] = z ? "su -V" : "su -v";
                                strArr[1] = "exit";
                                oVar.mo152a(strArr, arrayList, null, false);
                            } catch (C0064r unused) {
                            }
                            list = arrayList;
                        } catch (C0064r unused2) {
                        }
                    }
                    if (list != null) {
                        Iterator it = list.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            str2 = (String) it.next();
                            if (!z) {
                                if (str2.trim().equals("")) {
                                }
                                break;
                            }
                            try {
                                if (Integer.parseInt(str2) <= 0) {
                                }
                                break;
                                break;
                            } catch (NumberFormatException unused3) {
                                continue;
                            }
                        }
                    }
                    str2 = null;
                    break;
                    f222b[c] = str2;
                    break;
                }
                str = f222b[c];
            }
            return str;
        }

        @Deprecated
        /* renamed from: a */
        public static List<String> m241a(String str) {
            return C0032c.m116a("su", new String[]{str}, null, false);
        }

        @Deprecated
        /* renamed from: a */
        public static List<String> m242a(List<String> list) {
            return C0032c.m116a("su", (String[]) list.toArray(new String[0]), null, false);
        }

        /* renamed from: a */
        public static synchronized void m243a() {
            synchronized (C0063q.class) {
                f221a = null;
                f222b[0] = null;
                f222b[1] = null;
            }
        }

        /* renamed from: b */
        public static boolean m244b(String str) {
            int indexOf = str.indexOf(32);
            if (indexOf >= 0) {
                str = str.substring(0, indexOf);
            }
            int lastIndexOf = str.lastIndexOf(47);
            if (lastIndexOf >= 0) {
                str = str.substring(lastIndexOf + 1);
            }
            return str.toLowerCase(Locale.ENGLISH).equals("su");
        }
    }

    /* renamed from: eu.chainfire.b.c$r */
    public static class C0064r extends Exception {
        public C0064r() {
            super("Shell died (or access was not granted)");
        }
    }

    /* renamed from: eu.chainfire.b.c$s */
    public static class C0065s extends RuntimeException {
        public C0065s() {
            super("Application did not close() interactive shell");
        }
    }

    /* renamed from: eu.chainfire.b.c$t */
    public static class C0066t extends RuntimeException {
        public C0066t(String str) {
            super(str);
        }
    }

    /* renamed from: eu.chainfire.b.c$u */
    public static class C0067u extends C0035c {

        /* renamed from: e */
        private static int f223e;
        /* access modifiers changed from: private */

        /* renamed from: f */
        public final HandlerThread f224f = ((HandlerThread) this.f157a.getLooper().getThread());

        /* renamed from: g */
        private final boolean f225g;

        /* renamed from: h */
        private final Object f226h = new Object();

        /* renamed from: i */
        private volatile boolean f227i = false;

        /* renamed from: j */
        private final Object f228j = new Object();

        /* renamed from: k */
        private volatile boolean f229k = false;

        /* renamed from: l */
        private volatile boolean f230l = true;

        /* renamed from: m */
        private volatile boolean f231m = false;

        protected C0067u(C0033a aVar, C0055m mVar, boolean z) {
            super(aVar.mo108a(m252l()).mo116b(true).mo117c(true), mVar);
            this.f225g = z;
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public void m248b(boolean z) {
            if (this.f225g) {
                synchronized (this.f228j) {
                    if (!this.f229k) {
                        this.f229k = true;
                        C0056n.m230d(this);
                    }
                }
                if (z) {
                    this.f231m = true;
                }
            }
            super.mo130e();
        }

        /* access modifiers changed from: private */
        /* renamed from: c */
        public void m250c(boolean z) {
            this.f230l = z;
        }

        /* renamed from: k */
        private static int m251k() {
            int i;
            synchronized (C0067u.class) {
                i = f223e;
                f223e++;
            }
            return i;
        }

        /* renamed from: l */
        private static Handler m252l() {
            StringBuilder sb = new StringBuilder();
            sb.append("Shell.Threaded#");
            sb.append(m251k());
            HandlerThread handlerThread = new HandlerThread(sb.toString());
            handlerThread.start();
            return new Handler(handlerThread.getLooper());
        }

        /* access modifiers changed from: private */
        /* renamed from: m */
        public boolean m253m() {
            return this.f230l;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo124a(boolean z) {
            if (this.f225g) {
                if (z) {
                    synchronized (this.f228j) {
                        if (!this.f229k) {
                            C0056n.m229c(this);
                        }
                        if (this.f231m) {
                            super.mo124a(true);
                        }
                    }
                    return;
                }
                synchronized (this.f228j) {
                    if (!this.f229k) {
                        this.f229k = true;
                        C0056n.m230d(this);
                    }
                }
                z = false;
            }
            super.mo124a(z);
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0022, code lost:
            super.mo127c();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x002b, code lost:
            if (r3.f224f.isAlive() != false) goto L_0x002e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x002d, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x002e, code lost:
            r3.f157a.post(new p002eu.chainfire.p005b.C0032c.C0067u.C00681(r3));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0038, code lost:
            return;
         */
        /* renamed from: c */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void mo127c() {
            /*
                r3 = this;
                boolean r0 = r3.f225g
                r1 = 1
                if (r0 == 0) goto L_0x0016
                java.lang.Object r0 = r3.f228j
                monitor-enter(r0)
                boolean r2 = r3.f229k     // Catch:{ all -> 0x0013 }
                if (r2 != 0) goto L_0x0011
                r3.f229k = r1     // Catch:{ all -> 0x0013 }
                p002eu.chainfire.p005b.C0032c.C0056n.m230d(r3)     // Catch:{ all -> 0x0013 }
            L_0x0011:
                monitor-exit(r0)     // Catch:{ all -> 0x0013 }
                goto L_0x0016
            L_0x0013:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0013 }
                throw r1
            L_0x0016:
                java.lang.Object r0 = r3.f226h
                monitor-enter(r0)
                boolean r2 = r3.f227i     // Catch:{ all -> 0x0039 }
                if (r2 == 0) goto L_0x001f
                monitor-exit(r0)     // Catch:{ all -> 0x0039 }
                return
            L_0x001f:
                r3.f227i = r1     // Catch:{ all -> 0x0039 }
                monitor-exit(r0)     // Catch:{ all -> 0x0039 }
                super.mo127c()
                android.os.HandlerThread r0 = r3.f224f
                boolean r0 = r0.isAlive()
                if (r0 != 0) goto L_0x002e
                return
            L_0x002e:
                android.os.Handler r0 = r3.f157a
                eu.chainfire.b.c$u$1 r1 = new eu.chainfire.b.c$u$1
                r1.<init>()
                r0.post(r1)
                return
            L_0x0039:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0039 }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.p005b.C0032c.C0067u.mo127c():void");
        }

        public void close() {
            if (this.f225g) {
                super.mo130e();
            } else {
                mo130e();
            }
        }

        /* renamed from: e */
        public void mo130e() {
            m248b(false);
        }

        /* access modifiers changed from: protected */
        public void finalize() {
            if (this.f225g) {
                this.f158b = true;
            }
            super.finalize();
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: j */
        public synchronized boolean mo156j() {
            return this.f229k;
        }
    }

    @TargetApi(19)
    /* renamed from: eu.chainfire.b.c$v */
    public static class C0069v extends C0067u implements AutoCloseable {
        protected C0069v(C0033a aVar, C0055m mVar, boolean z) {
            super(aVar, mVar, z);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(19:10|(7:12|13|(3:15|(2:17|58)(1:59)|18)|57|19|(2:22|20)|60)|23|(1:25)(1:26)|27|28|29|(1:31)|61|32|41|42|43|44|45|(1:51)(1:50)|52|55|56) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:44:0x0170 */
    @java.lang.Deprecated
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<java.lang.String> m116a(java.lang.String r17, java.lang.String[] r18, java.lang.String[] r19, boolean r20) {
        /*
            r1 = r17
            r0 = r18
            r2 = r19
            r3 = r20
            java.util.Locale r4 = java.util.Locale.ENGLISH
            java.lang.String r4 = r1.toUpperCase(r4)
            boolean r5 = p002eu.chainfire.p005b.C0029a.m109c()
            if (r5 == 0) goto L_0x0028
            boolean r5 = p002eu.chainfire.p005b.C0029a.m111d()
            if (r5 != 0) goto L_0x001b
            goto L_0x0028
        L_0x001b:
            java.lang.String r0 = "Application attempted to run a shell command from the main thread"
            p002eu.chainfire.p005b.C0029a.m102a(r0)
            eu.chainfire.b.c$t r0 = new eu.chainfire.b.c$t
            java.lang.String r1 = "Application attempted to run a shell command from the main thread"
            r0.<init>(r1)
            throw r0
        L_0x0028:
            boolean r5 = f126b
            if (r5 == 0) goto L_0x0035
            eu.chainfire.b.c$o r1 = p002eu.chainfire.p005b.C0032c.C0056n.m227b(r17)
            java.util.List r0 = r1.mo155a(r0, r2, r3)
            return r0
        L_0x0035:
            java.util.Locale r5 = java.util.Locale.ENGLISH
            java.lang.String r6 = "[%s%%] START"
            r7 = 1
            java.lang.Object[] r8 = new java.lang.Object[r7]
            r9 = 0
            r8[r9] = r4
            java.lang.String r5 = java.lang.String.format(r5, r6, r8)
            p002eu.chainfire.p005b.C0029a.m105b(r5)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.List r5 = java.util.Collections.synchronizedList(r5)
            if (r2 == 0) goto L_0x00b8
            java.util.HashMap r8 = new java.util.HashMap     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.util.Map r10 = java.lang.System.getenv()     // Catch:{ IOException | InterruptedException -> 0x018d }
            r8.<init>(r10)     // Catch:{ IOException | InterruptedException -> 0x018d }
            int r10 = r2.length     // Catch:{ IOException | InterruptedException -> 0x018d }
            r11 = 0
        L_0x005c:
            if (r11 >= r10) goto L_0x0078
            r12 = r2[r11]     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.String r13 = "="
            int r13 = r12.indexOf(r13)     // Catch:{ IOException | InterruptedException -> 0x018d }
            if (r13 < 0) goto L_0x0075
            java.lang.String r14 = r12.substring(r9, r13)     // Catch:{ IOException | InterruptedException -> 0x018d }
            int r13 = r13 + 1
            java.lang.String r12 = r12.substring(r13)     // Catch:{ IOException | InterruptedException -> 0x018d }
            r8.put(r14, r12)     // Catch:{ IOException | InterruptedException -> 0x018d }
        L_0x0075:
            int r11 = r11 + 1
            goto L_0x005c
        L_0x0078:
            int r2 = r8.size()     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.util.Set r8 = r8.entrySet()     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.util.Iterator r8 = r8.iterator()     // Catch:{ IOException | InterruptedException -> 0x018d }
            r10 = 0
        L_0x0087:
            boolean r11 = r8.hasNext()     // Catch:{ IOException | InterruptedException -> 0x018d }
            if (r11 == 0) goto L_0x00b8
            java.lang.Object r11 = r8.next()     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.util.Map$Entry r11 = (java.util.Map.Entry) r11     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IOException | InterruptedException -> 0x018d }
            r12.<init>()     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.Object r13 = r11.getKey()     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ IOException | InterruptedException -> 0x018d }
            r12.append(r13)     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.String r13 = "="
            r12.append(r13)     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.Object r11 = r11.getValue()     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ IOException | InterruptedException -> 0x018d }
            r12.append(r11)     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.String r11 = r12.toString()     // Catch:{ IOException | InterruptedException -> 0x018d }
            r2[r10] = r11     // Catch:{ IOException | InterruptedException -> 0x018d }
            int r10 = r10 + 1
            goto L_0x0087
        L_0x00b8:
            java.lang.Runtime r8 = java.lang.Runtime.getRuntime()     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.Process r2 = r8.exec(r1, r2)     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.io.DataOutputStream r8 = new java.io.DataOutputStream     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.io.OutputStream r10 = r2.getOutputStream()     // Catch:{ IOException | InterruptedException -> 0x018d }
            r8.<init>(r10)     // Catch:{ IOException | InterruptedException -> 0x018d }
            eu.chainfire.b.d r10 = new eu.chainfire.b.d     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ IOException | InterruptedException -> 0x018d }
            r11.<init>()     // Catch:{ IOException | InterruptedException -> 0x018d }
            r11.append(r4)     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.String r12 = "-"
            r11.append(r12)     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.String r11 = r11.toString()     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.io.InputStream r12 = r2.getInputStream()     // Catch:{ IOException | InterruptedException -> 0x018d }
            r10.<init>(r11, r12, r5)     // Catch:{ IOException | InterruptedException -> 0x018d }
            eu.chainfire.b.d r11 = new eu.chainfire.b.d     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IOException | InterruptedException -> 0x018d }
            r12.<init>()     // Catch:{ IOException | InterruptedException -> 0x018d }
            r12.append(r4)     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.String r13 = "*"
            r12.append(r13)     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.String r12 = r12.toString()     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.io.InputStream r13 = r2.getErrorStream()     // Catch:{ IOException | InterruptedException -> 0x018d }
            if (r3 == 0) goto L_0x00fe
            r3 = r5
            goto L_0x00ff
        L_0x00fe:
            r3 = 0
        L_0x00ff:
            r11.<init>(r12, r13, r3)     // Catch:{ IOException | InterruptedException -> 0x018d }
            r10.start()     // Catch:{ IOException | InterruptedException -> 0x018d }
            r11.start()     // Catch:{ IOException | InterruptedException -> 0x018d }
            int r3 = r0.length     // Catch:{ IOException -> 0x014f }
            r12 = 0
        L_0x010a:
            if (r12 >= r3) goto L_0x0140
            r13 = r0[r12]     // Catch:{ IOException -> 0x014f }
            java.util.Locale r14 = java.util.Locale.ENGLISH     // Catch:{ IOException -> 0x014f }
            java.lang.String r15 = "[%s+] %s"
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ IOException -> 0x014f }
            r6[r9] = r4     // Catch:{ IOException -> 0x014f }
            r6[r7] = r13     // Catch:{ IOException -> 0x014f }
            java.lang.String r6 = java.lang.String.format(r14, r15, r6)     // Catch:{ IOException -> 0x014f }
            p002eu.chainfire.p005b.C0029a.m105b(r6)     // Catch:{ IOException -> 0x014f }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x014f }
            r6.<init>()     // Catch:{ IOException -> 0x014f }
            r6.append(r13)     // Catch:{ IOException -> 0x014f }
            java.lang.String r13 = "\n"
            r6.append(r13)     // Catch:{ IOException -> 0x014f }
            java.lang.String r6 = r6.toString()     // Catch:{ IOException -> 0x014f }
            java.lang.String r13 = "UTF-8"
            byte[] r6 = r6.getBytes(r13)     // Catch:{ IOException -> 0x014f }
            r8.write(r6)     // Catch:{ IOException -> 0x014f }
            r8.flush()     // Catch:{ IOException -> 0x014f }
            int r12 = r12 + 1
            goto L_0x010a
        L_0x0140:
            java.lang.String r0 = "exit\n"
            java.lang.String r3 = "UTF-8"
            byte[] r0 = r0.getBytes(r3)     // Catch:{ IOException -> 0x014f }
            r8.write(r0)     // Catch:{ IOException -> 0x014f }
            r8.flush()     // Catch:{ IOException -> 0x014f }
            goto L_0x016a
        L_0x014f:
            r0 = move-exception
            java.lang.String r3 = r0.getMessage()     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.String r4 = "EPIPE"
            boolean r3 = r3.contains(r4)     // Catch:{ IOException | InterruptedException -> 0x018d }
            if (r3 != 0) goto L_0x016a
            java.lang.String r3 = r0.getMessage()     // Catch:{ IOException | InterruptedException -> 0x018d }
            java.lang.String r4 = "Stream closed"
            boolean r3 = r3.contains(r4)     // Catch:{ IOException | InterruptedException -> 0x018d }
            if (r3 == 0) goto L_0x0169
            goto L_0x016a
        L_0x0169:
            throw r0     // Catch:{ IOException | InterruptedException -> 0x018d }
        L_0x016a:
            r2.waitFor()     // Catch:{ IOException | InterruptedException -> 0x018d }
            r8.close()     // Catch:{ IOException -> 0x0170 }
        L_0x0170:
            r10.join()     // Catch:{ IOException | InterruptedException -> 0x018d }
            r11.join()     // Catch:{ IOException | InterruptedException -> 0x018d }
            r2.destroy()     // Catch:{ IOException | InterruptedException -> 0x018d }
            boolean r0 = p002eu.chainfire.p005b.C0032c.C0063q.m244b(r17)     // Catch:{ IOException | InterruptedException -> 0x018d }
            if (r0 == 0) goto L_0x0189
            int r0 = r2.exitValue()     // Catch:{ IOException | InterruptedException -> 0x018d }
            r2 = 255(0xff, float:3.57E-43)
            if (r0 != r2) goto L_0x0189
            r6 = 0
            goto L_0x018a
        L_0x0189:
            r6 = r5
        L_0x018a:
            r16 = r6
            goto L_0x018f
        L_0x018d:
            r16 = 0
        L_0x018f:
            java.util.Locale r0 = java.util.Locale.ENGLISH
            java.lang.String r2 = "[%s%%] END"
            java.lang.Object[] r3 = new java.lang.Object[r7]
            java.util.Locale r4 = java.util.Locale.ENGLISH
            java.lang.String r1 = r1.toUpperCase(r4)
            r3[r9] = r1
            java.lang.String r0 = java.lang.String.format(r0, r2, r3)
            p002eu.chainfire.p005b.C0029a.m105b(r0)
            return r16
        */
        throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.p005b.C0032c.m116a(java.lang.String, java.lang.String[], java.lang.String[], boolean):java.util.List");
    }

    /* renamed from: a */
    protected static boolean m118a(List<String> list, boolean z) {
        boolean z2 = false;
        if (list == null) {
            return false;
        }
        boolean z3 = false;
        for (String next : list) {
            if (next.contains("uid=")) {
                if (!z || next.contains("uid=0")) {
                    z2 = true;
                }
                return z2;
            } else if (next.contains("-BOC-")) {
                z3 = true;
            }
        }
        return z3;
    }
}
