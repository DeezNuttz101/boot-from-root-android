package p002eu.chainfire.p005b;

import android.os.Looper;
import android.os.Process;
import android.util.Log;

/* renamed from: eu.chainfire.b.a */
public class C0029a {

    /* renamed from: a */
    private static boolean f111a = false;

    /* renamed from: b */
    private static int f112b = 65535;

    /* renamed from: c */
    private static C0030a f113c = null;

    /* renamed from: d */
    private static boolean f114d = true;

    /* renamed from: eu.chainfire.b.a$a */
    public interface C0030a {
        /* renamed from: a */
        void mo99a(int i, String str, String str2);
    }

    /* renamed from: a */
    private static void m100a(int i, String str, String str2) {
        if (f111a && (f112b & i) == i) {
            if (f113c != null) {
                f113c.mo99a(i, str, str2);
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("[libsuperuser][");
            sb.append(str);
            sb.append("]");
            sb.append((str2.startsWith("[") || str2.startsWith(" ")) ? "" : " ");
            sb.append(str2);
            Log.d("libsuperuser", sb.toString());
        }
    }

    /* renamed from: a */
    public static void m101a(int i, boolean z) {
        int i2;
        if (z) {
            i2 = i | f112b;
        } else {
            i2 = (~i) & f112b;
        }
        f112b = i2;
    }

    /* renamed from: a */
    public static void m102a(String str) {
        m100a(1, "G", str);
    }

    /* renamed from: a */
    public static void m103a(boolean z) {
        f111a = z;
    }

    /* renamed from: a */
    public static boolean m104a() {
        return f111a;
    }

    /* renamed from: b */
    public static void m105b(String str) {
        m100a(2, "C", str);
    }

    /* renamed from: b */
    public static void m106b(boolean z) {
        f114d = z;
    }

    /* renamed from: b */
    public static boolean m107b() {
        return f114d;
    }

    /* renamed from: c */
    public static void m108c(String str) {
        m100a(4, "O", str);
    }

    /* renamed from: c */
    public static boolean m109c() {
        return m104a() && m107b();
    }

    /* renamed from: d */
    public static void m110d(String str) {
        m100a(8, "P", str);
    }

    /* renamed from: d */
    public static boolean m111d() {
        return (Looper.myLooper() == null || Looper.myLooper() != Looper.getMainLooper() || Process.myUid() == 0) ? false : true;
    }
}
