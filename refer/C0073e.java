package p002eu.chainfire.p005b;

import android.os.Build.VERSION;
import java.util.List;
import java.util.Locale;
import p002eu.chainfire.p005b.C0032c.C0062p;
import p002eu.chainfire.p005b.C0032c.C0066t;

/* renamed from: eu.chainfire.b.e */
public class C0073e {

    /* renamed from: a */
    private static final Object f241a = new Object();

    /* renamed from: b */
    private static volatile String f242b = null;

    /* renamed from: a */
    public static String m267a(String str, Object... objArr) {
        Locale locale;
        StringBuilder sb;
        String str2;
        if (VERSION.SDK_INT < 23) {
            locale = Locale.ENGLISH;
            sb = new StringBuilder();
        } else {
            if (f242b == null) {
                m268a();
            }
            str = str.trim();
            int indexOf = str.indexOf(32);
            String substring = indexOf >= 0 ? str.substring(0, indexOf) : str;
            String str3 = f242b;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(" ");
            sb2.append(substring);
            sb2.append(" ");
            if (str3.contains(sb2.toString())) {
                locale = Locale.ENGLISH;
                sb = new StringBuilder();
                str2 = "toybox ";
                sb.append(str2);
                sb.append(str);
                return String.format(locale, sb.toString(), objArr);
            }
            locale = Locale.ENGLISH;
            sb = new StringBuilder();
        }
        str2 = "toolbox ";
        sb.append(str2);
        sb.append(str);
        return String.format(locale, sb.toString(), objArr);
    }

    /* renamed from: a */
    public static void m268a() {
        if (f242b == null) {
            if (VERSION.SDK_INT < 23) {
                f242b = "";
            } else if (!C0029a.m109c() || !C0029a.m111d()) {
                synchronized (f241a) {
                    f242b = "";
                    List<String> a = C0062p.m238a((String) "toybox");
                    if (a != null) {
                        f242b = " ";
                        for (String trim : a) {
                            StringBuilder sb = new StringBuilder();
                            sb.append(f242b);
                            sb.append(trim.trim());
                            sb.append(" ");
                            f242b = sb.toString();
                        }
                    }
                }
            } else {
                C0029a.m102a((String) "Application attempted to init the Toolbox class from the main thread");
                throw new C0066t("Application attempted to init the Toolbox class from the main thread");
            }
        }
    }
}
