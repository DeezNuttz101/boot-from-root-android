package p002eu.chainfire.p003a.p004a;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import p002eu.chainfire.p003a.p004a.C0020b.C0021a;
import p002eu.chainfire.p003a.p004a.C0024d.C0026a;

/* renamed from: eu.chainfire.a.a.c */
public class C0022c extends C0024d {

    /* renamed from: a */
    protected volatile int f81a;

    /* renamed from: b */
    protected volatile int f82b;

    /* renamed from: c */
    protected volatile int f83c;

    /* renamed from: d */
    protected volatile int f84d;

    /* renamed from: e */
    protected volatile int f85e;

    /* renamed from: f */
    protected volatile int f86f;

    /* renamed from: g */
    protected volatile List<C0023a> f87g;

    /* renamed from: h */
    protected volatile List<Bitmap> f88h;

    /* renamed from: i */
    protected volatile boolean f89i;

    /* renamed from: j */
    protected final ReentrantLock f90j;

    /* renamed from: eu.chainfire.a.a.c$a */
    private class C0023a {

        /* renamed from: b */
        private final String f92b;

        /* renamed from: c */
        private final int f93c;

        /* renamed from: d */
        private Bitmap f94d = null;

        /* renamed from: e */
        private C0020b f95e = null;

        public C0023a(String str, int i) {
            this.f92b = str;
            this.f93c = i;
        }

        /* renamed from: a */
        public void mo89a() {
            if (this.f95e != null) {
                this.f95e.mo83c();
                this.f95e = null;
            }
            if (this.f94d != null) {
                C0022c.this.f88h.add(this.f94d);
                this.f94d = null;
            }
        }

        /* renamed from: b */
        public C0020b mo90b() {
            if (this.f94d == null) {
                if (C0022c.this.f88h.size() > 0) {
                    this.f94d = C0022c.this.f88h.remove(0);
                }
                this.f94d = C0022c.this.mo92a(this.f92b, this.f93c, C0022c.this.f83c, C0026a.LEFT, this.f94d);
            }
            if (this.f95e == null) {
                this.f95e = C0022c.this.mo93a(this.f94d);
            }
            return this.f95e;
        }

        /* renamed from: c */
        public void mo91c() {
            if (this.f94d != null) {
                C0022c.this.f88h.add(this.f94d);
                this.f94d = null;
            }
            if (this.f95e != null) {
                this.f95e.mo83c();
                this.f95e = null;
            }
        }
    }

    public C0022c(C0027e eVar, C0019a aVar, int i, int i2, int i3) {
        this(eVar, aVar, 0, 0, i, i2, 0, i3);
    }

    public C0022c(C0027e eVar, C0019a aVar, int i, int i2, int i3, int i4, int i5, int i6) {
        super(eVar, aVar, Typeface.MONOSPACE, i6);
        this.f87g = new ArrayList();
        this.f88h = new ArrayList();
        this.f89i = true;
        this.f90j = new ReentrantLock(true);
        mo86a(i, i2, i3, i4, i5, i6);
    }

    /* renamed from: a */
    private void m76a(C0023a aVar) {
        this.f90j.lock();
        try {
            this.f87g.add(aVar);
        } finally {
            this.f90j.unlock();
        }
    }

    /* renamed from: c */
    private void m77c() {
        this.f90j.lock();
        while (this.f87g.size() > this.f86f) {
            try {
                this.f87g.remove(0).mo89a();
            } finally {
                this.f90j.unlock();
            }
        }
    }

    /* renamed from: a */
    public void mo84a() {
        mo85a(1.0f);
    }

    /* renamed from: a */
    public void mo85a(float f) {
        this.f90j.lock();
        try {
            boolean a = this.f85e != 0 ? this.f97l.mo80a(this.f81a, this.f82b, this.f83c, this.f84d, this.f85e) : true;
            int i = (this.f82b + this.f84d) - this.f101p;
            for (int size = this.f87g.size() - 1; size >= 0; size--) {
                this.f97l.mo77a(this.f87g.get(size).mo90b(), this.f81a, i, this.f83c, this.f101p, C0021a.IMAGE, f);
                i -= this.f101p;
            }
            this.f97l.mo78a(a);
        } finally {
            this.f90j.unlock();
        }
    }

    /* JADX INFO: used method not loaded: eu.chainfire.a.a.d.a(int):null, types can be incorrect */
    /* renamed from: a */
    public void mo86a(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f90j.lock();
        try {
            super.mo94a(i6);
            if (i >= 0) {
                this.f81a = i;
            }
            if (i2 >= 0) {
                this.f82b = i2;
            }
            if (i3 >= 0) {
                this.f83c = i3;
            }
            if (i4 >= 0) {
                this.f84d = i4;
            }
            if (i5 >= 0) {
                this.f85e = i5;
            }
            if (i6 >= 0) {
                this.f86f = (int) Math.round(Math.ceil((double) (((float) this.f84d) / ((float) this.f101p))));
            }
            for (C0023a c : this.f87g) {
                c.mo91c();
            }
            for (Bitmap recycle : this.f88h) {
                recycle.recycle();
            }
            this.f88h.clear();
        } finally {
            this.f90j.unlock();
        }
    }

    /* renamed from: a */
    public void mo87a(String str, int i, boolean z) {
        if (str != null) {
            if (!str.equals("")) {
                while (str.length() > 0) {
                    int breakText = this.f98m.breakText(str, true, (float) (this.f83c - (this.f103r * 2)), null);
                    int indexOf = str.indexOf(10);
                    if (indexOf >= 0 && indexOf < breakText) {
                        m76a(new C0023a(str.substring(0, indexOf), i));
                        if (str.length() <= indexOf) {
                            break;
                        }
                        str = str.substring(indexOf + 1);
                        continue;
                    } else {
                        m76a(new C0023a(str.substring(0, breakText), i));
                        if (str.length() <= breakText) {
                            break;
                        }
                        str = str.substring(breakText);
                        continue;
                    }
                    if (!z) {
                        break;
                    }
                }
            } else {
                m76a(new C0023a("", i));
            }
            m77c();
        }
    }

    /* renamed from: b */
    public void mo88b() {
        this.f90j.lock();
        try {
            for (C0023a a : this.f87g) {
                a.mo89a();
            }
            this.f87g.clear();
            for (Bitmap recycle : this.f88h) {
                recycle.recycle();
            }
            this.f88h.clear();
        } finally {
            this.f90j.unlock();
        }
    }
}
