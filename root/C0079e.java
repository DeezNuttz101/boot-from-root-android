package p002eu.chainfire.librootjava;

import java.util.List;

/* renamed from: eu.chainfire.librootjava.e */
public class C0079e {

    /* renamed from: a */
    private static String[] f250a = {"allow appdomain supersu binder { call transfer }", "allow appdomain magisk binder { call transfer }"};

    /* renamed from: b */
    private static Boolean f251b = Boolean.valueOf(false);

    /* renamed from: a */
    public static void m293a(Boolean bool) {
        f251b = bool;
    }

    /* renamed from: a */
    public static void m294a(List<String> list) {
        if (f251b == null || !f251b.booleanValue()) {
            StringBuilder sb = new StringBuilder("supolicy --live");
            for (String append : f250a) {
                sb.append(" \"");
                sb.append(append);
                sb.append("\"");
            }
            sb.append(" >/dev/null 2>/dev/null");
            list.add(sb.toString());
            if (f251b != null) {
                f251b = Boolean.valueOf(true);
            }
        }
    }
}
