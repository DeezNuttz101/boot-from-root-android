package p002eu.chainfire.liveboot.p006a;

import java.io.File;
import p002eu.chainfire.p005b.C0032c.C0033a;
import p002eu.chainfire.p005b.C0032c.C0035c;
import p002eu.chainfire.p005b.C0070d.C0071a;

/* renamed from: eu.chainfire.liveboot.a.e */
public class C0139e {

    /* renamed from: a */
    private final C0035c f437a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final C0135c f438b;

    public C0139e(C0135c cVar, String str) {
        C0035c c;
        this.f438b = cVar;
        if (!new File(str).exists()) {
            c = null;
        } else {
            C0033a b = new C0033a().mo106a().mo109a((C0071a) new C0071a() {
                /* renamed from: a */
                public void mo143a(String str) {
                    C0139e.this.f438b.onLog(this, str);
                    C0139e.this.f438b.onLine(C0139e.this, str, -1);
                }
            }).mo115b((C0071a) new C0071a() {
                /* renamed from: a */
                public void mo143a(String str) {
                    C0139e.this.f438b.onLog(this, str);
                    C0139e.this.f438b.onLine(C0139e.this, str, -65536);
                }
            });
            StringBuilder sb = new StringBuilder();
            sb.append("sh ");
            sb.append(str);
            c = b.mo110a((Object) sb.toString()).mo118c();
        }
        this.f437a = c;
    }

    /* renamed from: a */
    public void mo255a() {
        final C0035c cVar = this.f437a;
        if (cVar != null) {
            new Thread(new Runnable() {
                public void run() {
                    cVar.mo131f();
                    cVar.close();
                }
            }).start();
        }
    }
}
