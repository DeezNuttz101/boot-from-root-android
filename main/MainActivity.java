package p002eu.chainfire.liveboot;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import p002eu.chainfire.librootjava.C0078d;

/* renamed from: eu.chainfire.liveboot.MainActivity */
public class MainActivity extends Activity {

    /* renamed from: a */
    private Handler f274a = new Handler();

    /* renamed from: b */
    private boolean f275b = true;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public int f276c = 0;

    /* renamed from: a */
    private int m328a() {
        Configuration configuration = getResources().getConfiguration();
        int rotation = ((WindowManager) getSystemService("window")).getDefaultDisplay().getRotation();
        return (((rotation == 0 || rotation == 2) && configuration.orientation == 2) || ((rotation == 1 || rotation == 3) && configuration.orientation == 1)) ? 2 : 1;
    }

    /* renamed from: a */
    public void mo187a(boolean z) {
        this.f275b = z;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1501 && intent != null && i2 == -1) {
            try {
                finish();
                startActivity(new Intent(this, MainActivity.class));
            } catch (Exception e) {
                C0078d.m287a(e);
            }
        }
        super.onActivityResult(i, i2, intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRequestedOrientation(m328a());
        setContentView(R.layout.activity_main);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.f275b = true;
        this.f276c++;
    }

    /* access modifiers changed from: protected */
    public void onUserLeaveHint() {
        super.onUserLeaveHint();
        if (this.f275b) {
            final int i = this.f276c;
            this.f274a.postDelayed(new Runnable() {
                public void run() {
                    if (MainActivity.this.f276c == i) {
                        MainActivity.this.finish();
                    }
                }
            }, 60000);
        }
    }
}
