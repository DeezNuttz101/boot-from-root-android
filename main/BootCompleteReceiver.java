package p002eu.chainfire.liveboot;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: eu.chainfire.liveboot.BootCompleteReceiver */
public class BootCompleteReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String str;
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            str = "boot_completed";
        } else if (intent.getAction().equals("android.intent.action.MY_PACKAGE_REPLACED")) {
            str = "package_replaced";
        } else {
            return;
        }
        BackgroundService.m326a(context, str);
    }
}
