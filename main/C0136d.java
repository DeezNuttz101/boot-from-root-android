package p002eu.chainfire.liveboot.p006a;

import android.opengl.GLES20;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Process;
import android.os.SystemClock;
import java.io.OutputStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import p002eu.chainfire.librootjava.C0078d;
import p002eu.chainfire.p003a.C0012a;
import p002eu.chainfire.p003a.C0012a.C0016b;
import p002eu.chainfire.p003a.p004a.C0019a;
import p002eu.chainfire.p003a.p004a.C0020b;
import p002eu.chainfire.p003a.p004a.C0022c;
import p002eu.chainfire.p003a.p004a.C0027e;
import p002eu.chainfire.p005b.C0029a;
import p002eu.chainfire.p005b.C0032c;
import p002eu.chainfire.p005b.C0032c.C0062p;
import p002eu.chainfire.p005b.C0073e;

/* renamed from: eu.chainfire.liveboot.a.d */
public class C0136d extends C0012a implements C0016b, C0135c {
    public static final String LIVEBOOT_ABORT_FILE = "/dev/.liveboot_exit";

    /* renamed from: a */
    private boolean f409a = false;

    /* renamed from: b */
    private int f410b = 0;

    /* renamed from: c */
    private int f411c = 0;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public int f412d = 80;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public boolean f413e = false;

    /* renamed from: f */
    private boolean f414f = false;

    /* renamed from: g */
    private boolean f415g = false;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public boolean f416h = true;

    /* renamed from: i */
    private boolean f417i = false;

    /* renamed from: j */
    private OutputStream f418j = null;

    /* renamed from: k */
    private ReentrantLock f419k = new ReentrantLock(true);

    /* renamed from: l */
    private String f420l = null;

    /* renamed from: m */
    private C0027e f421m = null;

    /* renamed from: n */
    private C0019a f422n = null;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public volatile C0022c f423o = null;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public C0131b f424p = null;

    /* renamed from: q */
    private C0127a f425q = null;

    /* renamed from: r */
    private C0139e f426r = null;

    /* renamed from: s */
    private HandlerThread f427s = null;

    /* renamed from: t */
    private Handler f428t = null;
    /* access modifiers changed from: private */

    /* renamed from: u */
    public long f429u = 0;
    /* access modifiers changed from: private */

    /* renamed from: v */
    public int f430v = 0;
    /* access modifiers changed from: private */

    /* renamed from: w */
    public volatile long f431w = 0;

    /* renamed from: f */
    private boolean m411f() {
        StringBuilder sb = new StringBuilder();
        sb.append("/system/bin/");
        sb.append(C0073e.m267a("ps", new Object[0]));
        sb.append(" | /system/bin/");
        sb.append(C0073e.m267a("grep", new Object[0]));
        sb.append(" bootanim | /system/bin/");
        sb.append(C0073e.m267a("grep", new Object[0]));
        sb.append(" -v grep");
        StringBuilder sb2 = new StringBuilder();
        sb2.append("/system/bin/");
        sb2.append(C0073e.m267a("ps", new Object[0]));
        sb2.append(" -A | /system/bin/");
        sb2.append(C0073e.m267a("grep", new Object[0]));
        sb2.append(" bootanim | /system/bin/");
        sb2.append(C0073e.m267a("grep", new Object[0]));
        sb2.append(" -v grep");
        List<String> a = C0062p.m239a(new String[]{sb.toString(), sb2.toString()});
        if (a != null) {
            for (String contains : a) {
                if (contains.contains("bootanim")) {
                    return true;
                }
            }
        }
        return C0143f.m429a("init.svc.bootanim", "stopped").equals("running");
    }

    /* renamed from: g */
    private void m413g() {
        StringBuilder sb = new StringBuilder();
        sb.append("/system/bin/");
        sb.append(C0073e.m267a("ps", new Object[0]));
        sb.append(" | /system/bin/");
        sb.append(C0073e.m267a("grep", new Object[0]));
        sb.append(" bootanim | /system/bin/");
        sb.append(C0073e.m267a("grep", new Object[0]));
        sb.append(" -v grep");
        StringBuilder sb2 = new StringBuilder();
        sb2.append("/system/bin/");
        sb2.append(C0073e.m267a("ps", new Object[0]));
        sb2.append(" -A | /system/bin/");
        sb2.append(C0073e.m267a("grep", new Object[0]));
        sb2.append(" bootanim | /system/bin/");
        sb2.append(C0073e.m267a("grep", new Object[0]));
        sb2.append(" -v grep");
        List<String> a = C0062p.m239a(new String[]{sb.toString(), sb2.toString()});
        if (a != null) {
            for (String split : a) {
                String[] split2 = split.split(" +");
                if (split2.length >= 2) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("/system/bin/");
                    sb3.append(C0073e.m267a("kill", new Object[0]));
                    sb3.append(" -9 ");
                    sb3.append(split2[1]);
                    C0032c.m116a("sh", new String[]{sb3.toString()}, null, false);
                }
            }
        }
    }

    /* renamed from: h */
    private void m414h() {
        String valueOf = String.valueOf(Process.myPid());
        StringBuilder sb = new StringBuilder();
        sb.append("/system/bin/");
        sb.append(C0073e.m267a("ps", new Object[0]));
        sb.append(" | /system/bin/");
        sb.append(C0073e.m267a("grep", new Object[0]));
        sb.append(" ");
        sb.append(valueOf);
        sb.append(" | /system/bin/");
        sb.append(C0073e.m267a("grep", new Object[0]));
        sb.append(" -v grep");
        StringBuilder sb2 = new StringBuilder();
        sb2.append("/system/bin/");
        sb2.append(C0073e.m267a("ps", new Object[0]));
        sb2.append(" -A | /system/bin/");
        sb2.append(C0073e.m267a("grep", new Object[0]));
        sb2.append(" ");
        sb2.append(valueOf);
        sb2.append(" | /system/bin/");
        sb2.append(C0073e.m267a("grep", new Object[0]));
        sb2.append(" -v grep");
        List<String> a = C0062p.m239a(new String[]{sb.toString(), sb2.toString()});
        if (a != null) {
            for (String split : a) {
                String[] split2 = split.split(" +");
                if (split2.length >= 3 && !split2[1].equals(valueOf) && split2[2].equals(valueOf)) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("/system/bin/");
                    sb3.append(C0073e.m267a("kill", new Object[0]));
                    sb3.append(" -9 ");
                    sb3.append(split2[1]);
                    C0032c.m116a("sh", new String[]{sb3.toString()}, null, false);
                }
            }
        }
    }

    /* renamed from: i */
    private void m416i() {
        StringBuilder sb = new StringBuilder();
        sb.append("/system/bin/");
        sb.append(C0073e.m267a("kill", new Object[0]));
        sb.append(" -9 ");
        sb.append(String.valueOf(Process.myPid()));
        C0062p.m239a(new String[]{sb.toString()});
    }

    public static void main(String[] strArr) {
        C0078d.m288a((String) "LiveBootSurface");
        C0078d.m291a(false);
        C0029a.m103a(false);
        C0029a.m101a(3, true);
        C0029a.m101a(4, false);
        C0029a.m106b(false);
        final UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
            public void uncaughtException(Thread thread, Throwable th) {
                C0078d.m289a("EXCEPTION", "%s", th.getClass().getName());
                if (defaultUncaughtExceptionHandler != null) {
                    defaultUncaughtExceptionHandler.uncaughtException(thread, th);
                } else {
                    System.exit(1);
                }
            }
        });
        new C0136d().run(strArr);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:76|77|78|80|81|82) */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0136, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0140, code lost:
        r1.f419k.unlock();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0145, code lost:
        throw r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:80:0x0138 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo61a() {
        /*
            r19 = this;
            r1 = r19
            boolean r0 = r1.f409a
            r2 = 0
            if (r0 == 0) goto L_0x000e
            r3 = 5000(0x1388, double:2.4703E-320)
            java.lang.Thread.sleep(r3)     // Catch:{ Exception -> 0x0108 }
            goto L_0x0108
        L_0x000e:
            long r3 = android.os.SystemClock.elapsedRealtime()
            r5 = 0
            r7 = r5
            r0 = 0
            r9 = 0
            r10 = 0
        L_0x0018:
            long r11 = android.os.SystemClock.elapsedRealtime()
            int r13 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r13 != 0) goto L_0x0038
            java.lang.String r13 = "service.bootanim.exit"
            java.lang.String r14 = "0"
            java.lang.String r13 = p002eu.chainfire.liveboot.p006a.C0143f.m429a(r13, r14)
            java.lang.String r14 = "1"
            boolean r13 = r13.equals(r14)
            if (r13 == 0) goto L_0x0038
            java.lang.String r7 = "service.bootanim.exit"
            java.lang.Object[] r8 = new java.lang.Object[r2]
            p002eu.chainfire.librootjava.C0078d.m290a(r7, r8)
            r7 = r11
        L_0x0038:
            int r13 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r13 != 0) goto L_0x0054
            java.lang.String r13 = "service.bootanim.completed"
            java.lang.String r14 = "0"
            java.lang.String r13 = p002eu.chainfire.liveboot.p006a.C0143f.m429a(r13, r14)
            java.lang.String r14 = "1"
            boolean r13 = r13.equals(r14)
            if (r13 == 0) goto L_0x0054
            java.lang.String r7 = "service.bootanim.completed"
            java.lang.Object[] r8 = new java.lang.Object[r2]
            p002eu.chainfire.librootjava.C0078d.m290a(r7, r8)
            r7 = r11
        L_0x0054:
            int r14 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r14 != 0) goto L_0x0070
            if (r0 != 0) goto L_0x0070
            long r15 = r11 - r3
            r17 = 1500(0x5dc, double:7.41E-321)
            int r15 = (r15 > r17 ? 1 : (r15 == r17 ? 0 : -1))
            if (r15 <= 0) goto L_0x0070
            boolean r15 = r19.m411f()
            if (r15 == 0) goto L_0x0070
            java.lang.String r0 = "bootAnimationSeen"
            java.lang.Object[] r15 = new java.lang.Object[r2]
            p002eu.chainfire.librootjava.C0078d.m290a(r0, r15)
            r0 = 1
        L_0x0070:
            r15 = 2500(0x9c4, double:1.235E-320)
            if (r0 == 0) goto L_0x008b
            if (r9 != 0) goto L_0x008b
            long r17 = r11 - r3
            int r17 = (r17 > r15 ? 1 : (r17 == r15 ? 0 : -1))
            if (r17 <= 0) goto L_0x008b
            boolean r17 = r19.m411f()
            if (r17 != 0) goto L_0x008b
            java.lang.String r9 = "bootAnimationGone"
            java.lang.Object[] r13 = new java.lang.Object[r2]
            p002eu.chainfire.librootjava.C0078d.m290a(r9, r13)
            r13 = 1
            goto L_0x008c
        L_0x008b:
            r13 = r9
        L_0x008c:
            if (r14 <= 0) goto L_0x00af
            if (r0 == 0) goto L_0x00af
            if (r13 != 0) goto L_0x00af
            if (r10 != 0) goto L_0x00af
            java.lang.String r9 = "bootAnimationkill"
            java.lang.Object[] r10 = new java.lang.Object[r2]
            p002eu.chainfire.librootjava.C0078d.m290a(r9, r10)
            r19.m413g()
            boolean r9 = r19.m411f()
            if (r9 != 0) goto L_0x00ae
            java.lang.String r9 = "bootAnimationkill/Gone"
            java.lang.Object[] r10 = new java.lang.Object[r2]
            p002eu.chainfire.librootjava.C0078d.m290a(r9, r10)
            r10 = 1
            r13 = 1
            goto L_0x00af
        L_0x00ae:
            r10 = 1
        L_0x00af:
            if (r14 != 0) goto L_0x00ca
            java.io.File r9 = new java.io.File
            java.lang.String r14 = "/dev/.liveboot_exit"
            r9.<init>(r14)
            boolean r9 = r9.exists()
            if (r9 == 0) goto L_0x00ca
            java.lang.String r0 = "bootCompleteAbortFromAPK"
            java.lang.Object[] r7 = new java.lang.Object[r2]
            p002eu.chainfire.librootjava.C0078d.m290a(r0, r7)
            r7 = r11
            r0 = 1
            r9 = 1
            r10 = 1
            goto L_0x00cb
        L_0x00ca:
            r9 = r13
        L_0x00cb:
            int r13 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r13 <= 0) goto L_0x00d1
            if (r0 == 0) goto L_0x00de
        L_0x00d1:
            if (r0 == 0) goto L_0x00d5
            if (r9 != 0) goto L_0x00de
        L_0x00d5:
            if (r0 == 0) goto L_0x0150
            if (r13 <= 0) goto L_0x0150
            long r11 = r11 - r7
            int r11 = (r11 > r15 ? 1 : (r11 == r15 ? 0 : -1))
            if (r11 <= 0) goto L_0x0150
        L_0x00de:
            java.lang.String r0 = "EXIT"
            java.lang.String r5 = "exit sequence"
            java.lang.Object[] r6 = new java.lang.Object[r2]
            p002eu.chainfire.librootjava.C0078d.m289a(r0, r5, r6)
            java.lang.String r0 = r1.f420l
            if (r0 == 0) goto L_0x00f5
            boolean r0 = r1.f409a
            if (r0 != 0) goto L_0x00f5
            r5 = 60000(0xea60, double:2.9644E-319)
            java.lang.Thread.sleep(r5)     // Catch:{ Exception -> 0x00f5 }
        L_0x00f5:
            java.lang.String r0 = "Runtime: %dms"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]
            long r6 = android.os.SystemClock.elapsedRealtime()
            long r6 = r6 - r3
            java.lang.Long r3 = java.lang.Long.valueOf(r6)
            r5[r2] = r3
            p002eu.chainfire.librootjava.C0078d.m290a(r0, r5)
        L_0x0108:
            long r3 = android.os.SystemClock.elapsedRealtime()
            r1.f431w = r3
            r1.f430v = r2
        L_0x0110:
            int r0 = r1.f412d
            int r0 = r0 * 5
            int r0 = r0 / 4
            r3 = 0
            if (r2 >= r0) goto L_0x0122
            java.lang.String r0 = ""
            r4 = -1
            r1.onLine(r3, r0, r4)
            int r2 = r2 + 1
            goto L_0x0110
        L_0x0122:
            r4 = 200(0xc8, double:9.9E-322)
            java.lang.Thread.sleep(r4)     // Catch:{ Exception -> 0x0127 }
        L_0x0127:
            boolean r0 = r1.f417i
            if (r0 == 0) goto L_0x0146
            java.util.concurrent.locks.ReentrantLock r0 = r1.f419k
            r0.lock()
            java.io.OutputStream r0 = r1.f418j     // Catch:{ Exception -> 0x0138 }
            r0.close()     // Catch:{ Exception -> 0x0138 }
            goto L_0x0138
        L_0x0136:
            r0 = move-exception
            goto L_0x0140
        L_0x0138:
            r1.f418j = r3     // Catch:{ all -> 0x0136 }
            java.util.concurrent.locks.ReentrantLock r0 = r1.f419k
            r0.unlock()
            goto L_0x0146
        L_0x0140:
            java.util.concurrent.locks.ReentrantLock r2 = r1.f419k
            r2.unlock()
            throw r0
        L_0x0146:
            r19.m413g()
            r19.m414h()
            r19.m416i()
            return
        L_0x0150:
            r11 = 64
            java.lang.Thread.sleep(r11)     // Catch:{ Exception -> 0x0018 }
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.liveboot.p006a.C0136d.mo61a():void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo62a(int i, int i2) {
        this.f410b = i;
        this.f411c = i2;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0178 A[SYNTHETIC, Splitter:B:82:0x0178] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0187  */
    /* JADX WARNING: Removed duplicated region for block: B:98:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo63a(java.lang.String[] r12) {
        /*
            r11 = this;
            java.lang.String r0 = "eu.chainfire.liveboot"
            r1 = 0
            r2 = 0
            p002eu.chainfire.librootjavadaemon.C0084a.m322a(r0, r2, r2, r1)
            p002eu.chainfire.p005b.C0073e.m268a()
            int r0 = r12.length
            r6 = r1
            r7 = r6
            r8 = r7
            r10 = r8
            r1 = 0
        L_0x0010:
            if (r1 >= r0) goto L_0x0127
            r3 = r12[r1]
            java.lang.String r4 = "test"
            boolean r4 = r3.equals(r4)     // Catch:{ Exception -> 0x011f }
            r5 = 1
            if (r4 == 0) goto L_0x002a
            r11.f409a = r5     // Catch:{ Exception -> 0x011f }
            java.lang.String r3 = "OPTS"
            java.lang.String r4 = "test==1"
            java.lang.Object[] r5 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x011f }
        L_0x0025:
            p002eu.chainfire.librootjava.C0078d.m289a(r3, r4, r5)     // Catch:{ Exception -> 0x011f }
            goto L_0x0123
        L_0x002a:
            java.lang.String r4 = "transparent"
            boolean r4 = r3.equals(r4)     // Catch:{ Exception -> 0x011f }
            if (r4 == 0) goto L_0x003b
            r11.f414f = r5     // Catch:{ Exception -> 0x011f }
            java.lang.String r3 = "OPTS"
            java.lang.String r4 = "transparent==1"
            java.lang.Object[] r5 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x011f }
            goto L_0x0025
        L_0x003b:
            java.lang.String r4 = "wordwrap"
            boolean r4 = r3.equals(r4)     // Catch:{ Exception -> 0x011f }
            if (r4 == 0) goto L_0x004c
            r11.f413e = r5     // Catch:{ Exception -> 0x011f }
            java.lang.String r3 = "OPTS"
            java.lang.String r4 = "wordwrap==1"
            java.lang.Object[] r5 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x011f }
            goto L_0x0025
        L_0x004c:
            java.lang.String r4 = "save"
            boolean r4 = r3.equals(r4)     // Catch:{ Exception -> 0x011f }
            if (r4 == 0) goto L_0x005d
            r11.f417i = r5     // Catch:{ Exception -> 0x011f }
            java.lang.String r3 = "OPTS"
            java.lang.String r4 = "save==1"
            java.lang.Object[] r5 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x011f }
            goto L_0x0025
        L_0x005d:
            java.lang.String r4 = "dark"
            boolean r4 = r3.equals(r4)     // Catch:{ Exception -> 0x011f }
            if (r4 == 0) goto L_0x006e
            r11.f415g = r5     // Catch:{ Exception -> 0x011f }
            java.lang.String r3 = "OPTS"
            java.lang.String r4 = "dark==1"
            java.lang.Object[] r5 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x011f }
            goto L_0x0025
        L_0x006e:
            java.lang.String r4 = "logcatnocolors"
            boolean r4 = r3.equals(r4)     // Catch:{ Exception -> 0x011f }
            if (r4 == 0) goto L_0x007f
            r11.f416h = r2     // Catch:{ Exception -> 0x011f }
            java.lang.String r3 = "OPTS"
            java.lang.String r4 = "logcatnocolors==1"
            java.lang.Object[] r5 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x011f }
            goto L_0x0025
        L_0x007f:
            java.lang.String r4 = "="
            boolean r4 = r3.contains(r4)     // Catch:{ Exception -> 0x011f }
            if (r4 == 0) goto L_0x0123
            r4 = 61
            int r9 = r3.indexOf(r4)     // Catch:{ Exception -> 0x011f }
            java.lang.String r9 = r3.substring(r2, r9)     // Catch:{ Exception -> 0x011f }
            int r4 = r3.indexOf(r4)     // Catch:{ Exception -> 0x011f }
            int r4 = r4 + r5
            java.lang.String r3 = r3.substring(r4)     // Catch:{ Exception -> 0x011f }
            java.lang.String r4 = "lines"
            boolean r4 = r9.equals(r4)     // Catch:{ Exception -> 0x011f }
            if (r4 == 0) goto L_0x00be
            r4 = 10
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3, r4)     // Catch:{ Exception -> 0x011f }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x011f }
            r11.f412d = r3     // Catch:{ Exception -> 0x011f }
            java.lang.String r3 = "OPTS"
            java.lang.String r4 = "mLines==%s"
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x011f }
            int r9 = r11.f412d     // Catch:{ Exception -> 0x011f }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x011f }
            r5[r2] = r9     // Catch:{ Exception -> 0x011f }
            goto L_0x0025
        L_0x00be:
            java.lang.String r4 = "logcatlevels"
            boolean r4 = r9.equals(r4)     // Catch:{ Exception -> 0x011f }
            if (r4 == 0) goto L_0x00d7
            java.lang.String r4 = "OPTS"
            java.lang.String r6 = "logcatLevelOpts==%s"
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x00d3 }
            r5[r2] = r3     // Catch:{ Exception -> 0x00d3 }
            p002eu.chainfire.librootjava.C0078d.m289a(r4, r6, r5)     // Catch:{ Exception -> 0x00d3 }
            r6 = r3
            goto L_0x0123
        L_0x00d3:
            r4 = move-exception
            r6 = r3
        L_0x00d5:
            r3 = r4
            goto L_0x0120
        L_0x00d7:
            java.lang.String r4 = "logcatbuffers"
            boolean r4 = r9.equals(r4)     // Catch:{ Exception -> 0x011f }
            if (r4 == 0) goto L_0x00ef
            java.lang.String r4 = "OPTS"
            java.lang.String r7 = "logcatBufferOpts==%s"
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x00ec }
            r5[r2] = r3     // Catch:{ Exception -> 0x00ec }
            p002eu.chainfire.librootjava.C0078d.m289a(r4, r7, r5)     // Catch:{ Exception -> 0x00ec }
            r7 = r3
            goto L_0x0123
        L_0x00ec:
            r4 = move-exception
            r7 = r3
            goto L_0x00d5
        L_0x00ef:
            java.lang.String r4 = "logcatformat"
            boolean r4 = r9.equals(r4)     // Catch:{ Exception -> 0x011f }
            if (r4 == 0) goto L_0x0107
            java.lang.String r4 = "OPTS"
            java.lang.String r8 = "logcatFormatOpt==%s"
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0104 }
            r5[r2] = r3     // Catch:{ Exception -> 0x0104 }
            p002eu.chainfire.librootjava.C0078d.m289a(r4, r8, r5)     // Catch:{ Exception -> 0x0104 }
            r8 = r3
            goto L_0x0123
        L_0x0104:
            r4 = move-exception
            r8 = r3
            goto L_0x00d5
        L_0x0107:
            java.lang.String r4 = "dmesg"
            boolean r4 = r9.equals(r4)     // Catch:{ Exception -> 0x011f }
            if (r4 == 0) goto L_0x0123
            java.lang.String r4 = "OPTS"
            java.lang.String r9 = "dmesgOpts==%s"
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x011c }
            r5[r2] = r3     // Catch:{ Exception -> 0x011c }
            p002eu.chainfire.librootjava.C0078d.m289a(r4, r9, r5)     // Catch:{ Exception -> 0x011c }
            r10 = r3
            goto L_0x0123
        L_0x011c:
            r4 = move-exception
            r10 = r3
            goto L_0x00d5
        L_0x011f:
            r3 = move-exception
        L_0x0120:
            p002eu.chainfire.librootjava.C0078d.m287a(r3)
        L_0x0123:
            int r1 = r1 + 1
            goto L_0x0010
        L_0x0127:
            java.io.File r12 = new java.io.File
            java.lang.String r0 = "/sbin/supersu/su.d/0000liveboot.script"
            r12.<init>(r0)
            boolean r12 = r12.exists()
            if (r12 == 0) goto L_0x0139
            java.lang.String r12 = "/sbin/supersu/su.d/0000liveboot.script"
        L_0x0136:
            r11.f420l = r12
            goto L_0x0159
        L_0x0139:
            java.io.File r12 = new java.io.File
            java.lang.String r0 = "/su/su.d/0000liveboot.script"
            r12.<init>(r0)
            boolean r12 = r12.exists()
            if (r12 == 0) goto L_0x0149
            java.lang.String r12 = "/su/su.d/0000liveboot.script"
            goto L_0x0136
        L_0x0149:
            java.io.File r12 = new java.io.File
            java.lang.String r0 = "/system/su.d/0000liveboot.script"
            r12.<init>(r0)
            boolean r12 = r12.exists()
            if (r12 == 0) goto L_0x0159
            java.lang.String r12 = "/system/su.d/0000liveboot.script"
            goto L_0x0136
        L_0x0159:
            android.os.HandlerThread r12 = new android.os.HandlerThread
            java.lang.String r0 = "LiveBoot HandlerThread"
            r12.<init>(r0)
            r11.f427s = r12
            android.os.HandlerThread r12 = r11.f427s
            r12.start()
            android.os.Handler r12 = new android.os.Handler
            android.os.HandlerThread r0 = r11.f427s
            android.os.Looper r0 = r0.getLooper()
            r12.<init>(r0)
            r11.f428t = r12
            boolean r12 = r11.f417i
            if (r12 == 0) goto L_0x0183
            java.io.FileOutputStream r12 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0182 }
            java.lang.String r0 = "/cache/liveboot.log"
            r12.<init>(r0, r2)     // Catch:{ Exception -> 0x0182 }
            r11.f418j = r12     // Catch:{ Exception -> 0x0182 }
            goto L_0x0183
        L_0x0182:
        L_0x0183:
            java.lang.String r12 = r11.f420l
            if (r12 != 0) goto L_0x01a3
            eu.chainfire.liveboot.a.b r12 = new eu.chainfire.liveboot.a.b
            int r0 = r11.f412d
            int r5 = r0 * 4
            android.os.Handler r9 = r11.f428t
            r3 = r12
            r4 = r11
            r3.<init>(r4, r5, r6, r7, r8, r9)
            r11.f424p = r12
            eu.chainfire.liveboot.a.a r12 = new eu.chainfire.liveboot.a.a
            int r0 = r11.f412d
            int r0 = r0 * 4
            android.os.Handler r1 = r11.f428t
            r12.<init>(r11, r0, r10, r1)
            r11.f425q = r12
        L_0x01a3:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.liveboot.p006a.C0136d.mo63a(java.lang.String[]):void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo64b() {
        this.f427s.quit();
        if (this.f424p != null) {
            this.f424p.mo249b();
        }
        if (this.f425q != null) {
            this.f425q.mo246b();
        }
        if (this.f426r != null) {
            this.f426r.mo255a();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo65b(int i, int i2) {
        this.f410b = i;
        this.f411c = i2;
        this.f423o.mo86a(-1, -1, i, i2, -1, this.f411c / this.f412d);
        this.f422n.mo76a(i, i2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo66c() {
        this.f421m = new C0027e();
        this.f422n = new C0019a(this.f410b, this.f411c, C0019a.m64a());
        C0022c cVar = new C0022c(this.f421m, this.f422n, this.f410b, this.f411c, this.f411c / this.f412d);
        this.f423o = cVar;
        C0020b.m70a();
        if (this.f420l == null) {
            if (this.f425q != null) {
                this.f425q.mo245a();
            }
            if (this.f424p != null) {
                this.f424p.mo248a();
                return;
            }
            return;
        }
        this.f426r = new C0139e(this, this.f420l);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo67d() {
        this.f423o.mo88b();
        this.f423o = null;
        this.f421m.mo98b();
        this.f421m = null;
    }

    public void onGLRenderFrame() {
        GLES20.glDisable(3042);
        float f = 1.0f;
        if (this.f431w > 0) {
            f = 1.0f - (((float) (SystemClock.elapsedRealtime() - this.f431w)) / 200.0f);
        }
        float f2 = 0.0f;
        if (!this.f414f) {
            if (!this.f415g) {
                f2 = f * 0.2f;
            }
            GLES20.glClearColor(f2, f2, f2, f);
        } else {
            GLES20.glClearColor(0.0f, 0.0f, 0.0f, (this.f415g ? 0.75f : 0.25f) * f);
        }
        GLES20.glClear(16384);
        GLES20.glEnable(3042);
        this.f423o.mo84a();
    }

    public void onLine(final Object obj, final String str, final int i) {
        this.f428t.post(new Runnable() {
            public void run() {
                long j;
                if (C0136d.this.f423o != null) {
                    if (C0136d.this.f431w == 0) {
                        if (C0136d.this.f429u == 0) {
                            C0136d.this.f429u = SystemClock.elapsedRealtime();
                        }
                        j = C0136d.this.f429u;
                    } else {
                        j = C0136d.this.f431w;
                    }
                    C0136d.this.f430v = C0136d.this.f430v + 1;
                    while (SystemClock.elapsedRealtime() - j < ((long) Math.min(200, (int) ((((float) C0136d.this.f430v) / ((float) C0136d.this.f412d)) * 200.0f)))) {
                        try {
                            Thread.sleep(1);
                        } catch (Exception unused) {
                        }
                    }
                    if (C0136d.this.f431w == 0) {
                        int i = i;
                        if (obj == C0136d.this.f424p && !C0136d.this.f416h) {
                            i = -1;
                        }
                        C0136d.this.f423o.mo87a(str, i, C0136d.this.f413e);
                        return;
                    }
                    C0136d.this.f423o.mo87a("", -1, C0136d.this.f413e);
                }
            }
        });
    }

    public void onLog(Object obj, String str) {
        if (this.f417i) {
            this.f419k.lock();
            try {
                if (this.f418j != null) {
                    try {
                        OutputStream outputStream = this.f418j;
                        StringBuilder sb = new StringBuilder();
                        sb.append(str);
                        sb.append("\n");
                        outputStream.write(sb.toString().getBytes());
                    } catch (Exception unused) {
                    }
                }
            } finally {
                this.f419k.unlock();
            }
        }
    }
}
