package p002eu.chainfire.liveboot.p006a;

import android.os.Handler;
import android.os.SystemClock;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;
import p002eu.chainfire.librootjava.C0078d;
import p002eu.chainfire.p005b.C0032c.C0033a;
import p002eu.chainfire.p005b.C0032c.C0035c;
import p002eu.chainfire.p005b.C0070d.C0071a;

/* renamed from: eu.chainfire.liveboot.a.a */
public class C0127a {

    /* renamed from: a */
    private volatile int f367a = 0;

    /* renamed from: b */
    private volatile int f368b = 99;

    /* renamed from: c */
    private final C0035c f369c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public final C0135c f370d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public volatile long f371e = 0;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public volatile boolean f372f = false;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public final LinkedList<String> f373g = new LinkedList<>();
    /* access modifiers changed from: private */

    /* renamed from: h */
    public final int f374h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public volatile boolean f375i = false;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public final ReentrantLock f376j = new ReentrantLock(true);

    public C0127a(C0135c cVar, int i, String str, Handler handler) {
        if (str != null) {
            int indexOf = str.indexOf(45);
            if (indexOf > -1) {
                try {
                    this.f367a = Integer.valueOf(str.substring(0, indexOf), 10).intValue();
                    this.f368b = Integer.valueOf(str.substring(indexOf + 1), 10).intValue();
                } catch (Exception e) {
                    this.f367a = 0;
                    this.f368b = 99;
                    C0078d.m287a(e);
                }
            }
        }
        this.f370d = cVar;
        this.f374h = i;
        this.f369c = new C0033a().mo113a(false).mo108a(handler).mo106a().mo109a((C0071a) new C0071a() {
            /* renamed from: a */
            public void mo143a(String str) {
                C0127a.this.f370d.onLog(this, str);
                try {
                    C0127a.this.f376j.lock();
                    if (!C0127a.this.f372f) {
                        long uptimeMillis = SystemClock.uptimeMillis();
                        if (C0127a.this.f371e <= 0 || uptimeMillis - C0127a.this.f371e <= 16) {
                            C0127a.this.f371e = uptimeMillis;
                        } else {
                            if (C0127a.this.f375i) {
                                C0127a.this.f372f = true;
                                C0127a.this.m376c();
                            }
                            C0127a.this.f371e = 1;
                        }
                    }
                    if (C0127a.this.f372f) {
                        C0127a.this.m373a(str);
                    } else {
                        if (C0127a.this.f373g.size() >= C0127a.this.f374h) {
                            C0127a.this.f373g.pop();
                        }
                        C0127a.this.f373g.add(str);
                    }
                    C0127a.this.f376j.unlock();
                } catch (Exception e) {
                    C0078d.m287a(e);
                } catch (Throwable th) {
                    C0127a.this.f376j.unlock();
                    throw th;
                }
            }
        }).mo115b((C0071a) new C0071a() {
            /* renamed from: a */
            public void mo143a(String str) {
                C0127a.this.f370d.onLog(this, str);
                C0078d.m289a("dmesg/stderr", "%s", str);
            }
        }).mo110a((Object) "cat /dev/kmsg").mo110a((Object) "cat /proc/kmsg").mo118c();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        if (r1 <= r12.f368b) goto L_0x0031;
     */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m373a(java.lang.String r13) {
        /*
            r12 = this;
            int r0 = r13.length()
            if (r0 <= 0) goto L_0x0096
            r0 = 0
            java.lang.String r1 = "<"
            boolean r1 = r13.startsWith(r1)
            r2 = 10
            r3 = 1
            r4 = -1
            if (r1 == 0) goto L_0x0033
            r1 = 62
            int r1 = r13.indexOf(r1)
            if (r1 <= r4) goto L_0x008f
            java.lang.String r1 = r13.substring(r3, r1)     // Catch:{ Exception -> 0x0028 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1, r2)     // Catch:{ Exception -> 0x0028 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0028 }
            goto L_0x0029
        L_0x0028:
            r1 = -1
        L_0x0029:
            int r2 = r12.f367a
            if (r1 < r2) goto L_0x008f
            int r2 = r12.f368b
            if (r1 > r2) goto L_0x008f
        L_0x0031:
            r0 = r13
            goto L_0x008f
        L_0x0033:
            r1 = 59
            int r1 = r13.indexOf(r1)
            if (r1 <= r4) goto L_0x008f
            int r1 = r1 + r3
            java.lang.String r1 = r13.substring(r1)
            java.lang.String r5 = ","
            java.lang.String[] r13 = r13.split(r5)
            if (r13 == 0) goto L_0x008f
            int r5 = r13.length
            r6 = 3
            if (r5 < r6) goto L_0x008f
            r5 = 0
            r7 = r13[r5]     // Catch:{ NumberFormatException -> 0x008e }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r7, r2)     // Catch:{ NumberFormatException -> 0x008e }
            int r2 = r2.intValue()     // Catch:{ NumberFormatException -> 0x008e }
            int r7 = r12.f367a     // Catch:{ NumberFormatException -> 0x008e }
            if (r2 < r7) goto L_0x008f
            int r7 = r12.f368b     // Catch:{ NumberFormatException -> 0x008e }
            if (r2 > r7) goto L_0x008f
            r7 = 2
            r13 = r13[r7]     // Catch:{ NumberFormatException -> 0x008e }
            int r8 = r13.length()     // Catch:{ NumberFormatException -> 0x008e }
            int r8 = r8 + -6
            java.lang.String r8 = r13.substring(r5, r8)     // Catch:{ NumberFormatException -> 0x008e }
            int r9 = r13.length()     // Catch:{ NumberFormatException -> 0x008e }
            int r9 = r9 + -6
            java.lang.String r13 = r13.substring(r9)     // Catch:{ NumberFormatException -> 0x008e }
            java.util.Locale r9 = java.util.Locale.ENGLISH     // Catch:{ NumberFormatException -> 0x008e }
            java.lang.String r10 = "<%d>[%s.%6s] %s"
            r11 = 4
            java.lang.Object[] r11 = new java.lang.Object[r11]     // Catch:{ NumberFormatException -> 0x008e }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ NumberFormatException -> 0x008e }
            r11[r5] = r2     // Catch:{ NumberFormatException -> 0x008e }
            r11[r3] = r8     // Catch:{ NumberFormatException -> 0x008e }
            r11[r7] = r13     // Catch:{ NumberFormatException -> 0x008e }
            r11[r6] = r1     // Catch:{ NumberFormatException -> 0x008e }
            java.lang.String r13 = java.lang.String.format(r9, r10, r11)     // Catch:{ NumberFormatException -> 0x008e }
            goto L_0x0031
        L_0x008e:
        L_0x008f:
            if (r0 == 0) goto L_0x0096
            eu.chainfire.liveboot.a.c r13 = r12.f370d
            r13.onLine(r12, r0, r4)
        L_0x0096:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.liveboot.p006a.C0127a.m373a(java.lang.String):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public synchronized void m376c() {
        Iterator it = this.f373g.iterator();
        while (it.hasNext()) {
            m373a((String) it.next());
        }
        this.f373g.clear();
    }

    /* renamed from: a */
    public void mo245a() {
        this.f376j.lock();
        try {
            this.f375i = true;
            m376c();
        } finally {
            this.f376j.unlock();
        }
    }

    /* renamed from: b */
    public void mo246b() {
        final C0035c cVar = this.f369c;
        new Thread(new Runnable() {
            public void run() {
                cVar.mo131f();
                cVar.close();
            }
        }).start();
    }
}
