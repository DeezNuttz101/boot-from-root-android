package p002eu.chainfire.liveboot.p006a;

import p002eu.chainfire.librootjava.C0078d;

/* renamed from: eu.chainfire.liveboot.a.f */
public class C0143f {
    /* renamed from: a */
    public static String m429a(String str, String str2) {
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", new Class[]{String.class, String.class}).invoke(null, new Object[]{str, str2});
        } catch (Exception e) {
            C0078d.m287a(e);
            return str2;
        }
    }
}
