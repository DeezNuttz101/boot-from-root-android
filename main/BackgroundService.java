package p002eu.chainfire.liveboot;

import android.content.Context;
import android.content.Intent;
import android.support.p000v4.app.C0000a;
import p002eu.chainfire.p005b.C0032c.C0063q;

/* renamed from: eu.chainfire.liveboot.BackgroundService */
public class BackgroundService extends C0000a {
    /* renamed from: a */
    public static void m326a(Context context, String str) {
        Intent intent = new Intent(context, BackgroundService.class);
        intent.putExtra("eu.chainfire.livebootanimation.EXTRA_ACTION", str);
        m6a(context, BackgroundService.class, 1000, intent);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo12a(Intent intent) {
        if ("boot_completed".equals(intent.getStringExtra("eu.chainfire.livebootanimation.EXTRA_ACTION"))) {
            C0063q.m241a((String) "echo 1 > /dev/.liveboot_exit");
        }
        if ("package_replaced".equals(intent.getStringExtra("eu.chainfire.livebootanimation.EXTRA_ACTION"))) {
            C0144b.m442c(this);
        }
    }
}
